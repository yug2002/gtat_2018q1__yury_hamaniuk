package com.epam.tat.test;

import com.epam.tat.framework.ui.Browser;
import com.epam.tat.framework.utils.DataProviderSource;
import com.epam.tat.framework.utils.Randoms;
import com.epam.tat.product.mailru.common.GlobalParameters;
import com.epam.tat.product.mailru.user.bo.User;
import com.epam.tat.product.mailru.user.bo.UserBuilder;
import com.epam.tat.product.mailru.user.bo.UserFactory;
import com.epam.tat.product.mailru.user.exception.MailruUserException;
import com.epam.tat.product.mailru.user.screen.LoginPage;
import com.epam.tat.product.mailru.user.service.UserService;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LoginTest {

    private UserService userService = new UserService(new LoginPage());
    private User defaultUser = UserFactory.createDefaultUser();
    private Browser browser;

//    @BeforeClass(alwaysRun = true)
//    public void setUp(){
//        browser = Browser.getInstance();
//        browser.navigate(GlobalParameters.DEFAULT_PRODUCT_URL);
//        Browser.getInstance().getWrappedDriver().manage().window().maximize();
//    }

    @BeforeMethod(alwaysRun = true)
    public void setUpMethod(){
        browser = Browser.getInstance();
        browser.navigate(GlobalParameters.DEFAULT_PRODUCT_URL);
        Browser.getInstance().getWrappedDriver().manage().window().maximize();
    }
//    @AfterMethod(alwaysRun = true)
//    public void back(){
//        userService.logout();
//    }

    @Test(description = "login as specific customer on mailru from login page")
    public void loginAsSpecificCustomerFromLoginPage()  {
        boolean result = false;
        String message ="";
        try{
            userService.login(defaultUser);
            result = true;
        }catch(MailruUserException ex){
            //System.out.println(ex.getMessage());
            message = ex.getMessage();
        }
        Assert.assertEquals(result, true, "Login failed..." + message);
    }

    @Test(description = "Login with incorrect login on mailru from login page"
            , expectedExceptions = MailruUserException.class)
    public void loginWithIncorrectLoginFromLoginPage() throws InterruptedException {
        User user = new UserBuilder()
                .userlogin(Randoms.randomIdentifier())
                .userpassword(GlobalParameters.DEFAULT_PASSWORD)
                .build();
        userService.login(user);
    }
    @Test(description="Login with incorrect password on mailru from login page"
            , expectedExceptions = MailruUserException.class)
    public void loginWithIncorrectPasswordFromLoginPage() throws InterruptedException {
        User user = new UserBuilder()
                .userlogin(GlobalParameters.DEFAULT_LOGIN)
                .userpassword(Randoms.randomIdentifier())
                .build();
        userService.login(user);
    }
    @Test(description = "Login with incorrect login and password on mailru from login page",
    dataProvider = "negative", dataProviderClass = DataProviderSource.class,
    expectedExceptions = MailruUserException.class)
    public void loginWithIncorrectLoginAndPasswordFromLoginPage(String login, String password){
        User user = new UserBuilder()
                .userlogin(login)
                .userpassword(password)
                .build();
        userService.login(user);
    }
    @Test(description = "Login with empty login and password on mailru from login page",
            expectedExceptions = MailruUserException.class, priority = 1)
    public void LoginWithEmptyLoginAndPasswordFromLoginPage(){
        User user = new UserBuilder()
                .userlogin(GlobalParameters.EMPTY)
                .userpassword(GlobalParameters.EMPTY)
                .build();
        userService.login(user);
    }


    @org.testng.annotations.AfterMethod(alwaysRun = true)
    public void killWebDriver(){
        browser.stopBrowser();
    }

}
