package com.epam.tat.test;

import com.epam.tat.framework.ui.Browser;
import com.epam.tat.product.mailru.common.GlobalParameters;
import com.epam.tat.product.mailru.letter.bo.Letter;
import com.epam.tat.product.mailru.letter.bo.LetterBuilder;
import com.epam.tat.product.mailru.letter.bo.LetterFactory;
import com.epam.tat.product.mailru.letter.exception.MailruLetterException;
import com.epam.tat.product.mailru.letter.service.LetterService;
import com.epam.tat.product.mailru.user.bo.UserFactory;
import com.epam.tat.product.mailru.user.screen.LoginPage;
import com.epam.tat.product.mailru.user.service.UserService;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class LetterTest {

    private LetterService letterService = new LetterService();
    private Letter defaultLetter = LetterFactory.createDefaultLetter();
    private Browser browser;

    @BeforeClass(alwaysRun = true)
    public void setUp(){
        browser = Browser.getInstance();
        browser.navigate(GlobalParameters.DEFAULT_PRODUCT_URL);
        new UserService(new LoginPage()).login(UserFactory.createDefaultUser());
    }

    @AfterClass(alwaysRun = true)
    public void killBrowser(){
        browser.stopBrowser();
    }

    @Test(description = "sending and receiving a letter with" +
            " correctly filled in: subject, address and text fields from " +
            "the profile page of the mailru site")
    public void checkSendingAndReceivingLetterWithCorrectDataFilled(){
        boolean result = letterService.send(defaultLetter);

        Assert.assertEquals(result, true, "letter did not send");
    }

    @Test(description = "check that mail does not send wthout filled-in address", expectedExceptions = MailruLetterException.class)
    public void checkThatMailDoesNotsendWithoutAddress(){
        Letter letter = new LetterBuilder()
                .letterTo(GlobalParameters.EMPTY)
                .letterTopic(GlobalParameters.DEFAULT_TOPIC)
                .letterText(GlobalParameters.DEFAULT_TEXT_LETTER)
                .build();
        letterService.send(letter);
    }
    @Test(description = "check that mail send without a topic and body")
    public void checkMailSendWithoutTopicAndBody(){
        boolean result = false;
        Letter letter = new LetterBuilder()
                .letterTo(GlobalParameters.DEFAULT_EMAIL)
                .letterTopic(GlobalParameters.EMPTY)
                .letterText(GlobalParameters.EMPTY)
                .build();
             result = letterService.send(letter);

        Assert.assertEquals(result, true, "letter did not send");
    }
    @Test(description = "check that the draft is created and deleted")
    public void checkThatDraftCreatedAndDeleted(){

        Letter letter = new LetterBuilder()
                .letterTo(GlobalParameters.EMPTY)
                .letterTopic(GlobalParameters.EMPTY)
                .letterText(GlobalParameters.DEFAULT_TEXT_LETTER)
                .build();
        String newLetter = letterService.createDraft(letter);
        boolean result = letterService.deleteDraft();

        Assert.assertEquals(newLetter!=null&result, true, "draft did not create or delete");
    }




}
