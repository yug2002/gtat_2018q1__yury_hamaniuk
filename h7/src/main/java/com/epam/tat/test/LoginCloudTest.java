package com.epam.tat.test;

import com.epam.tat.framework.runner.CommonTestRuntimeException;
import com.epam.tat.framework.ui.Browser;
import com.epam.tat.framework.utils.DataProviderSource;
import com.epam.tat.framework.utils.Randoms;
import com.epam.tat.product.mailru.cloud.login.exception.CloudLoginException;
import com.epam.tat.product.mailru.cloud.login.screen.LoginCloudPage;
import com.epam.tat.product.mailru.cloud.login.service.LoginCloudService;
import com.epam.tat.product.mailru.common.GlobalParameters;
import com.epam.tat.product.mailru.user.bo.User;
import com.epam.tat.product.mailru.user.bo.UserBuilder;
import com.epam.tat.product.mailru.user.bo.UserFactory;
import com.epam.tat.product.mailru.user.service.UserService;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LoginCloudTest {

    LoginCloudService loginCloudService = new LoginCloudService();
    UserService userService = new UserService(new LoginCloudPage());
    User defaultUser = UserFactory.createDefaultUser();

    @BeforeMethod
    public void openInstance(){
        Browser browser = Browser.getInstance();
        browser.navigate(GlobalParameters.DEFAULT_LOGIN_CLOUD_URL);
        loginCloudService.clickAuthorizeLink();
    }
    @AfterMethod
    public void back(){
        Browser.getInstance().stopBrowser();
    }

    @Test(description = "login as specific customer on cloud mailru from login page")
    public void loginAsSpecificCustomerOnCloudFromLoginPage(){
        boolean result = false;
        String message ="";
        try{
            //loginCloudService.clickAuthorizeLink();
            userService.login(defaultUser);
            //loginCloudService.login(defaultUser);
            result = true;
        }catch(CloudLoginException ex){
            System.out.println(ex.getMessage());
            message = ex.getMessage();
        }
        Assert.assertEquals(result, true, "Login failed..." + message);
    }
    @Test(description = "Login with incorrect login and correct password on cloud mailru from login page"
            , expectedExceptions = CommonTestRuntimeException.class)
    public void loginWithIncorrectLoginOnCloudFromLoginPage(){
        User user = new UserBuilder()
                .userlogin(Randoms.randomIdentifier())
                .userpassword(GlobalParameters.DEFAULT_PASSWORD)
                .build();
        userService.login(user);
    }
    @Test(description = "Login with correct login and incorrect password on cloud mailru from login page"
            , expectedExceptions = CommonTestRuntimeException.class)
    public void loginWithCorrectLoginOnCloudFromLoginPage(){
        User user = new UserBuilder()
                .userlogin(GlobalParameters.DEFAULT_LOGIN)
                .userpassword(Randoms.randomIdentifier())
                .build();
        userService.login(user);
    }
    @Test(description = "Login with empty login and empty password on cloud mailru from login page"
            , expectedExceptions = CommonTestRuntimeException.class)
    public void loginWithEmptyLoginAndPasswordOnCloudFromLoginPage(){
        User user = new UserBuilder()
                .userlogin(GlobalParameters.EMPTY)
                .userpassword(GlobalParameters.EMPTY)
                .build();
        userService.login(user);
    }
    @Test(description = "Login with empty login and empty password on cloud mailru from login page"
            , expectedExceptions = CommonTestRuntimeException.class)
    public void loginWithEmptyLoginOnCloudFromLoginPage(){
        User user = new UserBuilder()
                .userlogin(GlobalParameters.EMPTY)
                .userpassword(GlobalParameters.DEFAULT_PASSWORD)
                .build();
        userService.login(user);
    }
    @Test(description = "Login with correct login and empty password on cloud mailru from login page"
            , expectedExceptions = CommonTestRuntimeException.class)
    public void loginWithEmptyPasswordOnCloudFromLoginPage(){
        User user = new UserBuilder()
                .userlogin(GlobalParameters.DEFAULT_LOGIN)
                .userpassword(GlobalParameters.EMPTY)
                .build();
        userService.login(user);
    }

    @Test(description = "Login with incorrect login and password on cloud of mailru from login page",
            dataProvider = "negative", dataProviderClass = DataProviderSource.class,
            expectedExceptions = CommonTestRuntimeException.class)
    public void loginWithIncorrectLoginAndPasswordOnCloudFromLoginPage(String login, String password){
        User user = new UserBuilder()
                .userlogin(login)
                .userpassword(password)
                .build();
        userService.login(user);
    }



}
