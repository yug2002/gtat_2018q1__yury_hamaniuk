package com.epam.tat.product.mailru.user.service;

import com.epam.tat.product.mailru.common.screen.AbstractLoginPage;
import com.epam.tat.product.mailru.common.screen.ErrorAuthorizeScreen;
import com.epam.tat.product.mailru.common.screen.HeaderBarScreen;
import com.epam.tat.product.mailru.user.bo.User;
import com.epam.tat.product.mailru.user.exception.MailruUserException;

public class UserService {


    private AbstractLoginPage page;
    private HeaderBarScreen headerBarScreen = new HeaderBarScreen();
    private ErrorAuthorizeScreen errorAuthorizeScreen = new ErrorAuthorizeScreen();
    public UserService(AbstractLoginPage page){
        this.page = page;
    }

    public void login(User user) {
        unsafeLogin(user);
        checkLoginSuccsessfull();
    }

    public void unsafeLogin(User user) {
        page
            .open()
            .typeLogin(user.getLogin())
            .typePassword(user.getPassword())
            .clickInputButton();
    }

    public boolean isLoginSuccessfull() {
        return headerBarScreen.isUserLoggedIn();
    }

    public void checkLoginSuccsessfull() {
        String message = "";
        if (!isLoginSuccessfull()) {

            if (errorAuthorizeScreen.isExistErrorAuthorizeScreen()) {
                message = errorAuthorizeScreen.getTextError();
                page.back();
            } else
                message = page.getErrorMessage();
            throw new MailruUserException("error of login: " + message);
        }
    }


    public void logout() {
        headerBarScreen.clickLogout();
    }

}
