package com.epam.tat.product.mailru.letter.bo;

public class LetterBuilder {

    Letter letter = new Letter();

    public LetterBuilder letterTo(String address){
        letter.setTo(address);
        return this;
    }

    public LetterBuilder letterTopic(String topic){
        letter.setTopic(topic);
        return this;
    }
    public LetterBuilder letterText(String text){
        letter.setText(text);
        return this;
    }

    public Letter build(){
        return letter;
    }
}
