package com.epam.tat.product.mailru.user.bo;


import com.epam.tat.framework.utils.Randoms;
import com.epam.tat.product.mailru.common.GlobalParameters;

public class UserFactory {

    public static User createDefaultUser(){
        User user = new User();
        user.setLogin(GlobalParameters.DEFAULT_LOGIN);
        user.setPassword(GlobalParameters.DEFAULT_PASSWORD);
        return user;
    }

    public static User createUser(){
        User user = new User();
        user.setLogin(Randoms.randomIdentifier());
        user.setPassword(Randoms.randomIdentifier());
        return user;
    }

}
