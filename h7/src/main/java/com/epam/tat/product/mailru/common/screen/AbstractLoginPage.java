package com.epam.tat.product.mailru.common.screen;

import com.epam.tat.framework.ui.Browser;

public abstract class AbstractLoginPage {

    public abstract AbstractLoginPage open();
    public abstract AbstractLoginPage typeLogin(String login);
    public abstract AbstractLoginPage typePassword(String password);
    public abstract void clickInputButton();
    public abstract String getErrorMessage();
    public void back(){
        Browser.getInstance().getWrappedDriver().navigate().back();
    }
    //public abstract String getErrorMessage();
}
