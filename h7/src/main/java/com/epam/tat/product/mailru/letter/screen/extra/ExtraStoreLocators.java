package com.epam.tat.product.mailru.letter.screen.extra;

import lombok.Getter;
import org.openqa.selenium.By;

public class ExtraStoreLocators {
    @Getter
    private final By locatorButtonNewLetter = By.xpath("//*[@id='b-toolbar__left']//a");
    @Getter
    private final By locatorInputField = By.cssSelector("div.b-input_textarea>textarea[data-original-name='To']");
    @Getter
    private final By locatorTopicField = By.cssSelector("input.b-input[name='Subject']");
    @Getter
    private final By locatorTextField = By.xpath("//*[@id='tinymce']");
    @Getter
    private final By locatorSendButton = By.xpath("//span[@class='b-toolbar__btn__text']/../..");
    @Getter
    private final By locatorLinkInBox = By.xpath("//div[not(@class='message-sent__title')]/a[@href='/messages/inbox/'][not(@class='b-dropdown__list__item')]");
    @Getter
    private final By locatorLinkOutBox = By.xpath("//div/a[@href='/messages/sent/']");
    @Getter
    private final By locatorLinkLetterIn = By.cssSelector("div.b-datalist__item:nth-child(1) > div > a");
    @Getter
    private final By locatorTopicOpenedIncomigLetter = By.cssSelector(".b-letter__head__subj__text");
    @Getter
    private final By locatorWhoSentLetterInOpenedOutgoingLetter = By.cssSelector(".b-letter__head__addrs__value");
    @Getter
    private final By locatorAddressOpenedIncomingLetter = By.cssSelector(".b-letter__head__addrs__value");
    @Getter
    private final By locatorContentSubjectLinkLetterIncoming = By.cssSelector("div.b-datalist__item:nth-child(1) >" +
            " div > a div.b-datalist__item__subj");
    @Getter
    private final By locatorLinksAddressOut = By.xpath("//div[contains(@class, 'b-datalist_letters_to')]/" +
            "div[not(@class='b-datalist__head')]//a");
    @Getter
    private final By locatorTopicOut = By.xpath("//div[contains(@class, 'b-datalist_letters_to')]//a" +
            "//div[@class='b-datalist__item__subj']");
    @Getter
    private final By locatorDraft = By.xpath("//div/a[@href='/messages/drafts/']");
    @Getter
    private final By locatorLinkLetterInDrafts = By.xpath("//div[@id='YaDirectLine']/../../../../..//*[1][contains(@class,'b-datalist__item')]/*[@class='b-datalist__item__body']");
    @Getter
    private final By partialyLocatorCheckboxLetterInDraft = By.xpath("a/div[contains(@class,'js-item-checkbox')]");

    @Getter
    private final By locatorDeleteDraft = By.xpath("//*[@id='b-toolbar__right']//*[@class='b-toolbar__group']" +
            "//*[@class='b-toolbar__item']//div[contains(@class, 'b-toolbar__btn_grouped_last')]/i[contains(@class," +
            " 'ico_toolbar_remove')]/../..");
    @Getter
    private final By locatorTrash = By.xpath("//div[contains(@class,'js-href')]/a[@href='/messages/trash/']");

    @Getter
    private final By locatorLetterInTrash = By.xpath("//*[@id='b-letters']/*[@class='b-datalists']/div[not(contains(@style,'display:none'))] //*[@id='DoubleFormatLine']/../../../../..//*[@class='b-datalist__body']/*[1]");
    @Getter
    private final By locatorCheckboxLetterInTrash = By.xpath("//*[@id='b-letters']//*[@class='b-datalist__body']//a/div[contains(@class, 'b-datalist__item__cbx')]");
    @Getter
    private final By locatorTextInOpenedLetterOfTrash = By.cssSelector(".b-letter__details>.b-letter__body>div>div>div>div>div");
    @Getter
    private final By locatorButtonDeleteLetterFromTrash = By.xpath("//*[@id='b-toolbar__right']//i[contains(@class,'ico_toolbar_remove')]/..");
    @Getter
    private final By locatorIframe = By.cssSelector("tbody>tr>td>iframe");
    @Getter
    private final By locatorConfirmButtonForSendEmptyLetter = By.cssSelector("div#MailRuConfirm div.is-compose-empty_in>form>" +
            "div.popup__controls>button.confirm-ok");
}
