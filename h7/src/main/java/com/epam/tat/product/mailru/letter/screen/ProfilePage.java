package com.epam.tat.product.mailru.letter.screen;

import com.epam.tat.framework.ui.Browser;
import com.epam.tat.framework.ui.Element;
import com.epam.tat.product.mailru.letter.screen.extra.ExtraStoreLocators;
import lombok.Getter;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ProfilePage {
    private ExtraStoreLocators locators = new ExtraStoreLocators();
    @Getter
    private Element buttonNewLetter = new Element(locators.getLocatorButtonNewLetter());
    private Element inputField = new Element(locators.getLocatorInputField());
    private Element topicField = new Element(locators.getLocatorTopicField());
    private Element textField = new Element(locators.getLocatorTextField());
    private Element sendButton = new Element(locators.getLocatorSendButton());
    private Element linkIncomingBox = new Element(locators.getLocatorLinkInBox());
    private Element linkOutBox = new Element(locators.getLocatorLinkOutBox());
    private Element linkLetterIn = new Element(locators.getLocatorLinkLetterIn());
    private Element topicOpenedIncomingLetter = new Element(locators.getLocatorTopicOpenedIncomigLetter());
    private Element addressOpenedIncomingLetter = new Element(locators.getLocatorAddressOpenedIncomingLetter());
    private Element whoSentLetterInOpenedOutgoingLetter = new Element(locators.getLocatorWhoSentLetterInOpenedOutgoingLetter());
    private Element linksAddressOut = new Element(locators.getLocatorLinksAddressOut());
    //private Element topicOut = new Element(locators.getLocatorTopicOut());
    private Element linkDraft = new Element(locators.getLocatorDraft());
    private Element linkLetterInDrafts = new Element(locators.getLocatorLinkLetterInDrafts());
    private Element deleteDraft = new Element(locators.getLocatorDeleteDraft());
    private Element trash = new Element(locators.getLocatorTrash());
    private Element linkLetterInTrash = new Element(locators.getLocatorLetterInTrash());
    private Element textInOpenedLetterOfTrash = new Element(locators.getLocatorTextInOpenedLetterOfTrash());
    private Element checkboxLetterInTrash = new Element(locators.getLocatorCheckboxLetterInTrash());
    private Element buttonDeleteLetterFromTrash = new Element(locators.getLocatorButtonDeleteLetterFromTrash());
    private Element iFrame = new Element(locators.getLocatorIframe());
    private Element confirmButtonForSendEmptyLetter = new Element(locators.getLocatorConfirmButtonForSendEmptyLetter());


    public ProfilePage clickButtonNewLetter(){
        buttonNewLetter.click();
        return this;
    }

    public ProfilePage typeAddressInputField(String address){
        inputField.type(address);
        return this;
    }

    public ProfilePage typeTopicField(String topic){
        topicField.type(topic);
        return this;
    }

    public ProfilePage typeTextLetter(String text){
        iFrame.switchToFrame();
        textField.type(text);
        iFrame.switchToDefaultContent();
        return this;
    }
    public String getTextLetter(){
        String temp ="";
        iFrame.switchToFrame();
        temp = textField.getText();
        iFrame.switchToDefaultContent();
        return temp;
    }

    public ProfilePage clickSendButton(){
        sendButton.click();
        return this;
    }

    public void clickLinkIncomingBox() {

        linkIncomingBox.waitForAppear();
        linkIncomingBox = new Element(locators.getLocatorLinkInBox());

        for(int i=0; i<5;i++){
            try{
                //linkIncomingBox.waitForAppear();
                linkIncomingBox.click();
            }catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }


    }

    public void clickLinkIncomingLetter(){

        linkLetterIn.click();
    }
    public String getTextTopicOpenedIncomingLetter(){
        topicOpenedIncomingLetter.waitForAppear();
        topicOpenedIncomingLetter = new Element(locators.getLocatorTopicOpenedIncomigLetter());
        return topicOpenedIncomingLetter.getText();
    }
    public String getTextAddressOpenedIncomingLetter(){
        return addressOpenedIncomingLetter.getText();
    }

    public void clickLinkOutBox(){
        linkOutBox.click();
    }
    public List<WebElement> getListOfWebElementOfOutgoingMail(){
        linksAddressOut.waitForAppear();
        return linksAddressOut.findElements();
    }
    public String getTextWhoSentLetterInOpenedOutgoingLetter(){
        return whoSentLetterInOpenedOutgoingLetter.getText();
    }

    public ProfilePage clickConfirmButtonEmptyLetter(){
        confirmButtonForSendEmptyLetter.waitForAppear();
        confirmButtonForSendEmptyLetter.click();
        return this;
    }
    public ProfilePage clickLinkDraft(){
        linkDraft.waitForAppear();
        clickWithoutStaleElementReferenceException(linkDraft, 3);
        //linkDraft.click();
        return this;
    }
    public ProfilePage clickLinkLetterInDrafts(){
        linkLetterInDrafts.waitForAppear();
        clickWithoutStaleElementReferenceException(linkLetterInDrafts, 3);
        return this;
    }

    public ProfilePage clickCheckboxInDraft(){
        WebElement wrapCheckbox = linkLetterInDrafts.getWebElement();
        WebElement checkBox = wrapCheckbox.findElement(locators.getPartialyLocatorCheckboxLetterInDraft());
        checkBox.click();
        return this;
    }
    public String getTextLetterinTrash(){
        String str = null;

        linkLetterInTrash.waitForAppear();
        linkLetterInTrash.click();
        textInOpenedLetterOfTrash.waitForAppear();
        str = textInOpenedLetterOfTrash.getText();
        return str;
    }
    public boolean isLetterContainsText(String time){

        String r = getTextLetterinTrash();
        if(r.contains(time))
            return true;
        //boolean result = searchingElementByTextInCollectionWebElements(0, time);
        return false;
    }
    public void clickCheckboxLetterInTrash(){

        checkboxLetterInTrash = new Element(locators.getLocatorCheckboxLetterInTrash());
        List<WebElement> checkBoxes = checkboxLetterInTrash.findElements();
        for(WebElement el: checkBoxes){
            if(el.isDisplayed()){
                el.click();
                break;
            }
        }
    }

    public ProfilePage clickDeleteDraft(){
        deleteDraft.click();
        return this;
    }
    public ProfilePage clickLinkTrash(){
        trash.waitForAppear();
        clickWithoutStaleElementReferenceException(trash,3);
        //trash.click();
        return this;
    }

    public ProfilePage clickButtonDeleteLetterFromTrash(){
        List<WebElement> buttons = buttonDeleteLetterFromTrash.findElements();
        for(WebElement button: buttons){
            if(button.isDisplayed()){
                button.click();
                return this;
            }
        }
        return null;
    }
    private void clickWithoutStaleElementReferenceException(Element element, int count){
        for(int i= 0; i<count;i++){
            try{
                element.click();
                break;
            }catch (StaleElementReferenceException ex){
                System.out.println(ex.getMessage());
            }
        }

    }
    private boolean searchingElementByTextInCollectionWebElements(int numberElement, String curTime){
        String str=null;
        try{
            List<WebElement> letters = linkLetterInTrash.findElements();
            if(letters.size()<=numberElement|letters.size()==0){
                return false;
            }

            if(letters.get(numberElement).isDisplayed()){
                letters.get(numberElement).click();
                textInOpenedLetterOfTrash.waitForAppear();
                str = textInOpenedLetterOfTrash.getText();
                return str.contains(curTime);
            }
            else {
                searchingElementByTextInCollectionWebElements(numberElement+1, curTime);
            }

        }catch(StaleElementReferenceException ex){
            searchingElementByTextInCollectionWebElements( numberElement, curTime);
            System.out.println(ex.getMessage());
        }
        return searchingElementByTextInCollectionWebElements(numberElement+1, curTime);
    }


    public void back(){
        Browser.getInstance().getWrappedDriver().navigate().back();
    }




}
