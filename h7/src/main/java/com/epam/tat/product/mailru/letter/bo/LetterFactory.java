package com.epam.tat.product.mailru.letter.bo;

import com.epam.tat.product.mailru.common.GlobalParameters;

public class LetterFactory {

    public static Letter createDefaultLetter(){
        Letter letter = new Letter();
        letter.setTo(GlobalParameters.DEFAULT_EMAIL);
        letter.setTopic(GlobalParameters.DEFAULT_TOPIC);
        letter.setText(GlobalParameters.DEFAULT_TEXT_LETTER);
        return letter;
    }

}
