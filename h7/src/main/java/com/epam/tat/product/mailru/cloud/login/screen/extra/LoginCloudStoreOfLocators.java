package com.epam.tat.product.mailru.cloud.login.screen.extra;

import lombok.Getter;
import org.openqa.selenium.By;

public class LoginCloudStoreOfLocators {
    @Getter
    private final By locatorAuthLink = By.cssSelector("a#PH_authLink");
    @Getter
    private final By locatorLoginField = By.cssSelector("input[name='Login']");
    @Getter
    private final By locatorPasswordField =By.cssSelector("input#ph_password");
    @Getter
    private final By locatorInputButton = By.cssSelector("input[type='submit']");
    @Getter
    private final By locatorErrorfield = By.cssSelector("#PH_user-email");
}
