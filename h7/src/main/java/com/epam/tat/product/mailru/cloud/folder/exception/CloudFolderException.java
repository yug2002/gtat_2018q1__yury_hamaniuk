package com.epam.tat.product.mailru.cloud.folder.exception;

import com.epam.tat.framework.runner.CommonTestRuntimeException;

public class CloudFolderException extends CommonTestRuntimeException {

    public CloudFolderException(String message) {
        super(message);
    }
}
