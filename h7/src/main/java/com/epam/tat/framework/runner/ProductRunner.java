package com.epam.tat.framework.runner;

import org.testng.TestNG;

import java.util.Arrays;
import java.util.List;

public class ProductRunner {

    public static void main(String[] args){

        TestNG testNG = new TestNG();
        List<String> filesXml = Arrays.asList("./src/main/resources/suites/mailruSuite.xml", "./src/main/resources/suites/cloudSuite.xml");
        testNG.setTestSuites(filesXml);
        testNG.run();


    }
}
