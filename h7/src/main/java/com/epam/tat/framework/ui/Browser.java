package com.epam.tat.framework.ui;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.util.List;

public class Browser implements WrapsDriver {
    private static Browser instance;
    private WebDriver wrappedWebDriver;
    private final static int ELEMENT_VISIBILITY_TIMEOUT_SECOND = 10;

    private Browser(){
        File driver = new File("./src/main/resources/webdriver/chromedriver.exe");
        System.setProperty("webdriver.chrome.driver",driver.getAbsolutePath());
        wrappedWebDriver = new ChromeDriver();
    }
    @Override
    public WebDriver getWrappedDriver() {
        return wrappedWebDriver;
    }

    public static Browser getInstance(){
        if(instance == null){
            instance = new Browser();
        }
        return instance;
    }

    public void navigate(String url){
        wrappedWebDriver.get(url);
    }

    public void uploadFile(File file, By fileInput){
        String pathToFile = file.getAbsolutePath();
//        waitForAppear(fileInput);
        wrappedWebDriver
                .findElement(fileInput)
                .sendKeys(pathToFile);
    }

    public void click(By by){
        for(int i=0; i<3; i++){
            try {
                wrappedWebDriver
                        .findElement(by)
                        .click();
                break;
            }catch (StaleElementReferenceException ex){
                System.out.println(ex.getMessage());
            }
        }
    }
    public void type(By by, String typeText){
        waitForAppear(by);
        wrappedWebDriver
                .findElement(by)
                .sendKeys(typeText);
    }
    public void clear(By by){
        waitForAppear(by);
        wrappedWebDriver
                .findElement(by)
                .clear();
    }
    public void waitForAppear(By by, int timeout){
        new WebDriverWait(wrappedWebDriver, timeout)
                .until(ExpectedConditions.visibilityOfElementLocated(by));
    }
    public void waitForAppear(By by){
        waitForAppear(by, ELEMENT_VISIBILITY_TIMEOUT_SECOND);
    }
    public void waitForClickable(By by, int timeout){
        new WebDriverWait(wrappedWebDriver, timeout)
        .until(ExpectedConditions.elementToBeClickable(by));
    }
    public void waitForPresent(By by){
        new WebDriverWait(wrappedWebDriver, ELEMENT_VISIBILITY_TIMEOUT_SECOND)
                .until(ExpectedConditions.presenceOfElementLocated(by));

    }
    public void waitForClickable(By by){
        waitForClickable(by, ELEMENT_VISIBILITY_TIMEOUT_SECOND);
    }

    public String getText(By by){
       // waitForAppear(by);
        return wrappedWebDriver.findElement(by).getText();
    }


    public boolean isVisible(By by){
        try{
            return wrappedWebDriver
                    .findElement(by)
                    .isDisplayed();
        }catch(NoSuchElementException ex){
            return false;
        }catch(StaleElementReferenceException ex){
            return false;
        }

    }

    public void switchToFrame(By by){
        WebElement el = wrappedWebDriver.findElement(by);
        new WebDriverWait(wrappedWebDriver, ELEMENT_VISIBILITY_TIMEOUT_SECOND)
                .until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(el));
    }
    public void switchToDefaultContent(){
        wrappedWebDriver.switchTo().defaultContent();
    }
    public String getTextAlert(){
        return wrappedWebDriver.switchTo().alert().getText();
    }

    public String getAttribute(By by, String attribute){
        return wrappedWebDriver.findElement(by).getAttribute(attribute);
    }
    public List<WebElement> findElements(By by){
        return wrappedWebDriver.findElements(by);
    }

    public void stopBrowser(){
        try{
            getInstance().getWrappedDriver().quit();
            //instance = null;
        }catch (WebDriverException ex){
            ex.printStackTrace();
        }finally {
            instance = null;
        }
    }

    public String getCurrentUrl(){
        return instance.getWrappedDriver().getCurrentUrl();
    }



}
