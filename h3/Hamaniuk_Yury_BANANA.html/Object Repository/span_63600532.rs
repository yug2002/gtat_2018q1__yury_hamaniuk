<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_63600532</name>
   <tag></tag>
   <elementGuidId>3360f2bf-d1f0-487c-9905-946274a286fd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[(text() = '63 600 532 просмотра' or . = '63 600 532 просмотра')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[@id = 'count']/yt-view-count-renderer/span)[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>view-count style-scope yt-view-count-renderer</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>63 600 532 просмотра</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;count&quot;)/yt-view-count-renderer[@class=&quot;style-scope ytd-video-primary-info-renderer&quot;]/span[@class=&quot;view-count style-scope yt-view-count-renderer&quot;]</value>
   </webElementProperties>
</WebElementEntity>
