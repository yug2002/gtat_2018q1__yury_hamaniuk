package com.epam.tat.framework.utils;

import org.testng.annotations.DataProvider;

public class DataProviderSource {

    private static Object[][] data;
    private static final int COUNT_ROW = 8;
    private static final int COUNT_COLUMN = 2;

    private static Object[][] generateData(){
        data = new Object[COUNT_ROW][COUNT_COLUMN];
        for(int i=0; i<COUNT_ROW; i++)
            for (int j = 0; j<COUNT_COLUMN; j++){
                data[i][j] =  Randoms.randomIdentifier() ;
            }

        return data;
    }

    @DataProvider(name = "negative")
    public static Object[][] getData(){
        return generateData();
    }
}
