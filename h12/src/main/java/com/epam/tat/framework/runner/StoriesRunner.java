package com.epam.tat.framework.runner;

import com.epam.tat.framework.steps.LetterTestSteps;
import com.epam.tat.framework.steps.LoginTestSteps;
import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.failures.FailingUponPendingStep;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.jbehave.core.steps.ParameterControls;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class StoriesRunner extends JUnitStories {

    @Test(description = "Run stories")
    @Override
    public void run() throws Throwable{
        super.run();
    }
    @Override
    protected List<String> storyPaths() {

        List<String> paths = new ArrayList<String>(){
            {add("stories/LoginTests.story");}
            {add("stories/LetterTests.story");}
        };
        return paths;
    }
    @Override
    public Configuration configuration(){
        return new MostUsefulConfiguration()
                .useParameterControls(new ParameterControls().useDelimiterNamedParameters(true))
                .useStoryLoader(new LoadFromClasspath())
                .usePendingStepStrategy(new FailingUponPendingStep())
                .useStoryReporterBuilder(new StoryReporterBuilder().withDefaultFormats().withFormats(Format.CONSOLE, Format.TXT));
    }
    @Override
    public InjectableStepsFactory stepsFactory(){
        return new InstanceStepsFactory(configuration(), new LoginTestSteps(), new LetterTestSteps());
    }


}
