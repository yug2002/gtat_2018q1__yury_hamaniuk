package com.epam.tat.framework.runner.validate;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;
import com.epam.tat.framework.runner.Parameters;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class SuiteValidate implements IParameterValidator {

    private static List<String> nameSuites = Arrays.asList(Parameters.PATH_MAILRU_SUITE, Parameters.PATH_CLOUD_SUITE);

    @Override
    public void validate(String s, String s1) throws ParameterException {
        if(s1==null || s1.isEmpty()){
            throw new ParameterException("Parameter " + s +" should contain data");
        }else {
            Optional<String> suites = nameSuites
                    .stream()
                    .filter(p->p.equals(s1))
                    .findFirst();
            if(suites.equals(Optional.empty())){
                throw new ParameterException("Parameter " + s +" should contain currect path " +
                        "to the suite (found "+ s1 +")");
            }
        }

    }
}
