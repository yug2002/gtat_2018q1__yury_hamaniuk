package com.epam.tat.framework.runner;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.Parameter;
import com.epam.tat.framework.runner.validate.SuiteValidate;
import com.epam.tat.framework.ui.BrowserType;
import org.testng.collections.Lists;
import org.testng.xml.XmlSuite;

import java.util.List;

public class Parameters {
    private static  Parameters instance;
    public static final String PATH_MAILRU_SUITE = "./src/main/resources/suites/mailruSuite.xml";
    public static final String PATH_CLOUD_SUITE = "./src/main/resources/suites/cloudSuite.xml";
    public static final String TESTS = "tests";
    public static final String METHODS = "methods";
    public static final String CLASSES = "classes";


    private static final String CHROME = "--chrome";
    @Parameter(names = {CHROME,"-c"}, description = "Path to google chrome driver")
    private String chromeDriver = "./src/main/resources/webdriver/chromedriver.exe";

    @Parameter(description = "The XML suite files to run")
    public List<String> suiteFiles = Lists.newArrayList();

    private static final String SUITE_NAME = "-suitename";
    @Parameter(names = {SUITE_NAME,"-s"}, description = "Default name of test suite, if not specified " +
            "in suite definition file or source code", validateWith = SuiteValidate.class)
    private String suiteName = PATH_MAILRU_SUITE;

    private static final String PARALLEL= "-parallel";
    @Parameter(names = PARALLEL, description = "Parallel mode (methods, tests or classes)")
    private XmlSuite.ParallelMode parallelMode;

    public static final String THREAD_COUNT = "-threadcount";
    @Parameter(names = THREAD_COUNT, description = "Number of threads to use when running tests " +
            "in parallel")
    public Integer threadCount;

    public static final String HELP = "--help";
    @Parameter(names = HELP, help = true, description = "How to use")
    private boolean help;

    public static final String BROWSER = "--browser";
    @Parameter(names = {BROWSER,"-b"}, description = "Browser type", converter = BrowserTypeConverter.class)
    private BrowserType browserType = BrowserType.CHROME;

    public String getChromeDriver(){return chromeDriver;}

    public Integer getThreadCount(){return threadCount;}

    public BrowserType getBrowserType(){ return browserType;}

    public String getSuite() {return suiteName;}

    public XmlSuite.ParallelMode getParallelMode(){
        return parallelMode;
    }

    public boolean isHelp(){ return help; }

    public static synchronized Parameters instance(){
        if(instance == null){
            instance = new Parameters();
        }
        return instance;
    }

    public static class BrowserTypeConverter implements IStringConverter<BrowserType>{

        @Override
        public BrowserType convert(String s) {
            return BrowserType.valueOf(s.toUpperCase());
        }
    }
}
