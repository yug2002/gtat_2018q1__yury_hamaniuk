package com.epam.tat.framework.utils;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class Randoms {
    private static final String lexicon = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ12345674890";
    private static final Random rand = new Random();
    private static final Set<String> identifiers = new HashSet<String>();

    public static String randomIdentifier() {
        final int start = 5;
        final int temp = 6;
        StringBuilder builder = new StringBuilder();
        while(builder.toString().length() == 0) {
            int length = rand.nextInt(start)+temp;
            for(int i = 0; i < length; i++) {
                builder.append(lexicon.charAt(rand.nextInt(lexicon.length())));
            }
            if(identifiers.contains(builder.toString())) {
                builder = new StringBuilder();
            }
        }
        return builder.toString();
    }
}
