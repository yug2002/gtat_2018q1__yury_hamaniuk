package com.epam.tat.framework.ui;

import org.openqa.selenium.*;

import java.io.File;
import java.util.List;

public class Element{

   protected By locator;
   public Element(By locator){
       this.locator = locator;
   }

   public void type(String typeText){
       Browser.getInstance().type(locator, typeText);
   }
   public void click(){
       Browser.getInstance().click(locator);
   }

    public void clear(){
       Browser.getInstance().clear(locator);
   }
    public String getText(){
       return Browser.getInstance().getText(locator);
   }

   public void waitForAppear(){
       Browser.getInstance().waitForAppear(locator);
   }
   public void waitForAppear(int timeout){
       Browser.getInstance().waitForAppear(locator, timeout);
   }
   public void waitForPresence(){
       Browser.getInstance().waitForPresent(locator);
   }
   public void waitForClickable(){Browser.getInstance().waitForClickable(locator);}
   public void switchToFrame(){
        Browser.getInstance().switchToFrame(locator);
   }
   public void switchToDefaultContent(){
       Browser.getInstance().switchToDefaultContent();
   }
   public List<WebElement> findElements(){
       return Browser.getInstance().findElements(locator);
   }
   public WebElement getWebElement(){
       return Browser.getInstance().getWrappedDriver().findElement(locator);
   }
   public String getAttribute(String attribute){
       return Browser.getInstance().getAttribute(locator, attribute);
   }
   public void uploading(File file){
       Browser.getInstance().uploadFile(file, locator);
   }

    public boolean isVisible(){
       //waitForAppear();
       return Browser.getInstance().isVisible(locator);
   }
   public boolean isPresent(){
       if(Browser.getInstance().findElements(locator).size()>0)
            return true;
       return false;
   }
}
