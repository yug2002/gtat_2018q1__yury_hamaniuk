package com.epam.tat.test.api.forecast;

public class GlobalWeatherParameters {
    //limits
    public static final int LIMIT_OF_HUMIDITY = 89;
    //coordinates
    public static final float LAT_GOMEL = 52.42f;
    public static final float LON_GOMEL = 31.01f;
    public static final float LAT_FEREYDUN_KENAR = 37;
    public static final float LON_FEREYDUN_KENAR = 52.01f;
    //names
    public static final String FEREYDUN_KENAR = "Fereydun Kenar";
    //id
    public static final int ID_MADRID = 6359304;
    //json keys Madrid
    public static final String LIST_MEMBER_NAME = "list";
    public static final String MAIN_MEMBER_NAME = "main";
    public static final String HUMIDITY_MEMBER_NAME = "humidity";




}
