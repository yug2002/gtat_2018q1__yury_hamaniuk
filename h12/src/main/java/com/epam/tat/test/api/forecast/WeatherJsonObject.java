package com.epam.tat.test.api.forecast;

import com.epam.tat.product.mailru.common.GlobalParameters;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.Getter;

import static io.restassured.RestAssured.when;

public class WeatherJsonObject {

    @Getter
    private Weather weather = new Weather();

    private String requestLocalityForecast;

    public void setRequestLocalityForecast(int idLocality){
        requestLocalityForecast = "forecast?id=" + idLocality + "&APPID={key}";
    }

    public Response getResponseLocalityForecast(){
        return when()
                .get(requestLocalityForecast, GlobalParameters.DEFAULT_API_KEY)
                .then()
                .contentType(ContentType.JSON)
                .extract()
                .response();
    }

    public JsonArray getJsonArray(){

        String jsonAsString = getResponseLocalityForecast().asString();
        JsonParser parser = new JsonParser();
        JsonObject mainObject = parser.parse(jsonAsString).getAsJsonObject();
        JsonArray list = mainObject.getAsJsonArray(GlobalWeatherParameters.LIST_MEMBER_NAME);
        return list;
    }

    public boolean isResultHumidityCorrect(){
        setHumidities();
        return weather.isMaxHumidityMoreThanLimit();

    }
    private void setHumidities(){
        for(JsonElement element: getJsonArray()){
            JsonObject mainJsonObject = (element.getAsJsonObject()).getAsJsonObject(GlobalWeatherParameters.MAIN_MEMBER_NAME);
            weather.getHumidities().add(mainJsonObject.get(GlobalWeatherParameters.HUMIDITY_MEMBER_NAME).getAsFloat());
        }
    }


}
