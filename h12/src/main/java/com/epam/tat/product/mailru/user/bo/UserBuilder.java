package com.epam.tat.product.mailru.user.bo;

public class UserBuilder {

    private User user = new User();

    public UserBuilder userlogin(String userlogin){
        user.setLogin(userlogin);
        return this;
    }

    public UserBuilder userpassword(String password){
        user.setPassword(password);
        return this;
    }
    public UserBuilder email(String email){
        user.setEmail(email);
        return this;
    }

    public User build(){
        return user;
    }
}
