package com.epam.tat.product.mailru.letter.bo;

import lombok.Getter;
import lombok.Setter;

public class Letter {
    @Getter
    @Setter
    private String topic;
    @Getter
    @Setter
    private String text;
    @Getter
    @Setter
    private String to;
//    @Override
//    public String toString(){
//        return ToStringBuilder.reflectionToString(this);
//    }

}
