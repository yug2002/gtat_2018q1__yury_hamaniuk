package com.epam.tat.product.mailru.cloud.login.service;

import com.epam.tat.product.mailru.cloud.login.screen.LoginCloudPage;
import com.epam.tat.product.mailru.common.screen.AbstractLoginPage;

public class LoginCloudService {

    AbstractLoginPage loginCloudPage = new LoginCloudPage();

    public void clickAuthorizeLink() {
        ((LoginCloudPage)loginCloudPage).clickAuthLink();
    }
}
