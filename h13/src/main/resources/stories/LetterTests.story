Feature: send Letter

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal


Scenario: sending and receiving a letter with correctly filled in: subject, address and text fields from
                      the profile page of the mailru site
Given I create default letter with correct data filled
When I send and receive letter with correct data filled
Then sending and receiving letters were successful

Scenario: mail does not send without filled-in address
Given I create default letter with empty address field
When I try to send letter without filled-in address
Then I can't to send this letter

Scenario: mail send without a topic and body
Given I create letter without a topic and body
When I try to send letter without a topic and body
Then I send this letter successfully

Scenario: a draft is created and deleted
Given I create letter only with text
When I try to create and delete draft
Then draft is created and deleted successfully

