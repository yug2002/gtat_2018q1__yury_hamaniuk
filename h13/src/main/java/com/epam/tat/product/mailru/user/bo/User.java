package com.epam.tat.product.mailru.user.bo;


import lombok.Getter;
import lombok.Setter;

public class User {
    @Getter
    @Setter
    private String login;
    @Getter
    @Setter
    private String password;
    @Getter
    @Setter
    private String email;
//    @Override
//    public String toString(){
//        return ToStringBuilder.reflectionToString(this);
//    }
}
