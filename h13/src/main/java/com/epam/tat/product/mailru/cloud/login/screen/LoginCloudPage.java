package com.epam.tat.product.mailru.cloud.login.screen;

import com.epam.tat.framework.logging.Log;
import com.epam.tat.framework.ui.Browser;
import com.epam.tat.framework.ui.Element;
import com.epam.tat.product.mailru.cloud.login.screen.extra.LoginCloudStoreOfLocators;
import com.epam.tat.product.mailru.common.screen.AbstractLoginPage;

import java.util.concurrent.TimeUnit;

public class LoginCloudPage extends AbstractLoginPage {

    LoginCloudStoreOfLocators locators = new LoginCloudStoreOfLocators();
    Element authLink = new Element(locators.getLocatorAuthLink());
    Element loginField = new Element(locators.getLocatorLoginField());
    Element passwordField = new Element(locators.getLocatorPasswordField());
    Element inputButton = new Element(locators.getLocatorInputButton());
    Element errorfield = new Element(locators.getLocatorErrorfield());

    public LoginCloudPage clickAuthLink() {
        authLink.click();
        return this;
    }

    @Override
    public AbstractLoginPage open() {
        Browser.getInstance().getWrappedDriver().manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        Browser.getInstance().getWrappedDriver().manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);
        Browser.getInstance().getWrappedDriver().manage().window().maximize();
        return this;
    }

    @Override
    public AbstractLoginPage typeLogin(String login) {
        loginField.clear();
        Log.debug("TYPE LOGIN - " + login);
        loginField.type(login);
        return this;
    }

    @Override
    public AbstractLoginPage typePassword(String password) {
        passwordField.clear();
        Log.debug("TYPE PASSWORD - " + password);
        passwordField.type(password);
        return this;
    }

    @Override
    public void clickInputButton(){
        inputButton.click();
    }
    @Override
    public String getErrorMessage() {
        if(hasErrorMessage())
            return errorfield.getText();
        return null;
    }

    private boolean hasErrorMessage() {
        return false;
    }

}
