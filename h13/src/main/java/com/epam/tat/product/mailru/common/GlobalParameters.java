package com.epam.tat.product.mailru.common;

public class GlobalParameters {
    public static final String DEFAULT_PRODUCT_URL = "https://mail.ru";
    public static final String DEFAULT_LOGIN_CLOUD_URL = "https://cloud.mail.ru/";
    public static final String DEFAULT_EMAIL = "yugtest2018@mail.ru";
    public static final String DEFAULT_LOGIN = "yugtest2018";
    public static final String DEFAULT_PASSWORD = "zxc123zxc";
    public static final String EMPTY = "";
    public static final String DEFAULT_TOPIC = "topic test";
    public static final String DEFAULT_TEXT_LETTER = "Hello, World!!!";
    public static final String DEFAULT_folderName = "MyFolder";
    public static final String RELATIVE_PATH_TO_FILE_TO_UPLOAD = "src/main/resources/files/web_driver_drawing_cat.gif";
    public static final String DEFAULT_FILE_NAME = "web_driver_drawing_cat";
    //Parametrs for API
    public static final String DEFAULT_API_URL = "http://api.openweathermap.org/data/2.5/";
    public static final String DEFAULT_API_KEY = "67e249621061b70a7e7693f94cd3b72a";
    public static final String ERROR_INVALID_API_KEY = "Invalid API key. Please see http://openweathermap.org/faq#error401 for more info.";

    public static final String REMOTE_WEBDRIVER_URL = "http://localhost:4444/wd/hub";


}
