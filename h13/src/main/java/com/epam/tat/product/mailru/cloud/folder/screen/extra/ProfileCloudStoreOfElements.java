package com.epam.tat.product.mailru.cloud.folder.screen.extra;

import com.epam.tat.framework.ui.Element;
import lombok.Getter;

public class ProfileCloudStoreOfElements {

    private ProfileCloudStoreOfLocators locators = new ProfileCloudStoreOfLocators();
    @Getter
    private Element createButton = new Element(locators.getLocatorCssCreateButton());
    @Getter
    private Element folderLink = new Element(locators.getLocatorCssFolderLink());
    @Getter
    private Element inputFolderName = new Element(locators.getLocatorCssInputFolderName());
    @Getter
    private Element submitCreateFolder = new Element(locators.getLocatorCssSubmitCreateFolder());
    @Getter
    private Element nameFolder = new Element(locators.getLocatorXpathNameFolder());
    @Getter
    private Element checkboxDelFolder = new Element(locators.getLocatorXpathCheckBoxDelFolder());
    @Getter
    private Element delFolder = new Element(locators.getLocatorXpathDelFolder());
    @Getter
    private Element popupButtonDel = new Element(locators.getLocalCssPopupButtonDel());
    @Getter
    private Element popupSendInTrashButton = new Element(locators.getLocalXpathPopupSendInTrashButton());
    @Getter
    private Element clearTrashButton = new Element(locators.getLocatorXpathClearTrashButton());
    @Getter
    private Element confirmClearTrashButton = new Element(locators.getLocatorCssConfirmClearTrashButton());
    @Getter
    private Element containsTrash = new Element(locators.getLocatorCssContainsTrash());
    @Getter
    private Element inputFile = new Element(locators.getLocatorCssInputFile());
    @Getter
    private Element uploadButton = new Element(locators.getLocatorXpathUploadButton());
    @Getter
    private Element uploadedFile = new Element(locators.getLocatorXpathNameOfUploadedFile());
    @Getter
    private Element advertising = new Element(locators.getLocatorCssAdvertising());
    @Getter
    private Element cloudHome = new Element(locators.getLocatorCssCloudHome());
    @Getter
    private Element containsFileInMyFolder = new Element((locators.getLocatorContainsMyFolder()));

    @Getter
    private Element linkToShare = new Element(locators.getLocatorCssGetLink());
    @Getter
    private Element buttonShare = new Element(locators.getLocatorCssButtonShare());
    @Getter
    private Element inputTextLink = new Element(locators.getLocatorCssInputTextLink());
    @Getter
    private Element popupWindowLinkToShare = new Element(locators.getLocatorCssPopupWindowLinkToShare());
    @Getter
    private Element frame = new Element(locators.getLocatorCssFrame());
    @Getter
    private Element textSharingLink = new Element(locators.getLocatorCssTextLetter());
    @Getter
    private Element droppable = new Element(locators.getLocatorXpathDroppableFolder());
    @Getter
    private Element moveButton = new Element(locators.getLocatorCssMoveButton());
    @Getter
    private Element draggableFile = new Element(locators.getLocatorXpathDraggableFile());
    @Getter
    private Element droppableTarget = new Element(locators.getLocatorXpathDroppableFolder());





}
