package com.epam.tat.product.mailru.common.screen;

import com.epam.tat.framework.ui.Browser;
import com.epam.tat.framework.ui.Element;
import org.openqa.selenium.By;

public class ErrorAuthorizeScreen {

    private static final By errorParagraph = By.cssSelector(".b-login__errors");
    private static final String startOfStringUrl = "https://account.mail.ru/";

    private Element errortext = new Element(errorParagraph);

    public String getCurrentUrl(){
        return Browser.getInstance().getCurrentUrl();
    }

    public boolean isExistErrorAuthorizeScreen(){

        if(getCurrentUrl().startsWith(startOfStringUrl)){
            return true;
        }
        return false;
    }

    public String getTextError(){
        return errortext.getText();

    }
}
