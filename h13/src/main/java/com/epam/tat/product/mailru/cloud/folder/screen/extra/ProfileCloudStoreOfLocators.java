package com.epam.tat.product.mailru.cloud.folder.screen.extra;

import com.epam.tat.product.mailru.common.GlobalParameters;
import lombok.Getter;
import org.openqa.selenium.By;

public class ProfileCloudStoreOfLocators {
    @Getter
    private final By locatorCssCreateButton = By.cssSelector("#toolbar-left .b-dropdown");
    @Getter
    private final By locatorCssFolderLink =By.cssSelector(".b-dropdown_expanded>div>a[data-name='folder']");
    @Getter
    private final By locatorCssInputFolderName = By.cssSelector("form.layer__form input");
    @Getter
    private final By locatorCssSubmitCreateFolder = By.cssSelector("form.layer__form button.btn_main");

    @Getter
    private final By locatorXpathNameFolder = By.xpath("//*[@class='b-filename__name' and text()='"+GlobalParameters.DEFAULT_folderName+"']");
    @Getter
    private final By locatorXpathNameOfUploadedFile = By.xpath("//*[@id='js-upload-panel']//*[@class='b-filename__name' and contains(text(),'"+GlobalParameters.DEFAULT_FILE_NAME+"')]");
    //*[@id='js-upload-panel']//*[@class='b-filename__name' and text()='web_driver_drawing_cat']
    @Getter
    private final By locatorXpathCheckBoxDelFolder =By.xpath("//*[text()='"
            + GlobalParameters.DEFAULT_folderName
            +"' and @class='b-filename__name']/../../../..//*[@class='b-checkbox__box']");
    @Getter
    private final By locatorXpathDelFolder = By.xpath("//*[contains(@class,'b-toolbar__btn_remove') and not(contains(@class,'b-toolbar__btn_disabled'))]");
    @Getter
    private final By localCssPopupButtonDel = By.cssSelector(".layer_remove .b-layer__controls__buttons>button.btn_main");
    @Getter
    private final By localXpathPopupSendInTrashButton = By.xpath("//*[@class='layer__footer']/button[not(contains(@class, 'btn_main'))]");
    @Getter
    private final By locatorXpathClearTrashButton =By.xpath("//div[contains(@class,'b-toolbar__btn_clear') and not(contains(@class,'b-toolbar__btn_disabled'))]");
    @Getter
    private final By locatorCssConfirmClearTrashButton = By.cssSelector(".layer_remove .b-layer__controls button[data-name='empty']");
    @Getter
    private final By locatorCssContainsTrash = By.cssSelector(".datalist-item__content_datalist-mode-trashbin .b-filename_icon .b-filename__name");
    @Getter
    private final By locatorCssInputFile = By.cssSelector(".layer_upload__controls input[type='file']");
    @Getter
    private final By locatorXpathUploadButton = By.xpath("//*[@id='cloud_toolbars']//*[contains(@class, 'ico_toolbar_upload')]/..");
    @Getter
    private final By locatorCssMoveButton = By.cssSelector("button[data-name='move']");
    @Getter
    private final By locatorCssFileInFolder = By.cssSelector(".b-collection__container_datalist-mode-thumb>div>div>.b-thumb_image");
    @Getter
    private final By locatorCssAdvertising = By.cssSelector(".b-layer__container_disko-promo .b-panel__close");
    @Getter
    private final By locatorCssCloudHome = By.cssSelector("#navmenu a[href='/home/");
    @Getter
    private final By locatorXpathDraggableFile = By.xpath("//*[contains(@data-id,'"+GlobalParameters.DEFAULT_FILE_NAME+".gif') and contains(@class, 'b-thumb')]");
    @Getter
    private final By locatorXpathDroppableFolder = By.xpath("//*[@class='b-filename__name' and text()='MyFolder']/../../../../..");
    @Getter
    private final By LocatorContainsMyFolder = By.xpath("//div[contains(@class,'b-filename')]/div[contains(text(),'"+GlobalParameters.DEFAULT_FILE_NAME+"')]/../../../..");

    //share
    @Getter
    private final By locatorCssGetLink = By.cssSelector("div#toolbar div[data-name='publish']");
    @Getter
    private final By locatorCssButtonShare= By.cssSelector("button[data-id='email']");
    @Getter
    private final By locatorCssInputTextLink = By.cssSelector("input[data-name='url']");
    @Getter
    private final By locatorCssPopupWindowLinkToShare = By.cssSelector(".b-layer__container.b-layer__container_publish button[data-name='close']");
    @Getter
    private final By locatorCssFrame = By.cssSelector("iframe[id]");
    @Getter
    private final By locatorCssTextLetter = By.cssSelector("#tinymce");


}
