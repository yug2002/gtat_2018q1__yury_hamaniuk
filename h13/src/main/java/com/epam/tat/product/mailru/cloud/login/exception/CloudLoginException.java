package com.epam.tat.product.mailru.cloud.login.exception;

import com.epam.tat.framework.runner.CommonTestRuntimeException;

public class CloudLoginException extends CommonTestRuntimeException {

    public CloudLoginException(String message) {
        super(message);
    }
}
