package com.epam.tat.product.mailru.cloud.folder.bo;

import lombok.Getter;
import lombok.Setter;

public class Folder {
    @Getter
    @Setter
    private String name;
}
