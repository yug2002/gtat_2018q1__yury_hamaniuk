package com.epam.tat.product.mailru.cloud.folder.bo;

import com.epam.tat.product.mailru.common.GlobalParameters;

public class FolderFactory {

    public static Folder createDefaultFolder(){
        Folder folder = new Folder();
        folder.setName(GlobalParameters.DEFAULT_folderName);
        return folder;
    }


}
