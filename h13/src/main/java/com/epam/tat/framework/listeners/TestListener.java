package com.epam.tat.framework.listeners;

import com.epam.tat.framework.logging.Log;
import com.epam.tat.framework.ui.Browser;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class TestListener extends TestListenerAdapter {

    @Override
    public void onStart(ITestContext testContext) {
        super.onStart(testContext);
        Log.info("[TESTCLASS STARTED] " + testContext.getName());
    }
    @Override
    public void onFinish(ITestContext testContext) {
        super.onFinish(testContext);
        Log.info("[TESTCLASS FINISHED]" + testContext.getName());
        Log.debug("Quits the driver, closing every associated window.");
    }
    @Override
    public void onTestFailure(ITestResult tr) {
        super.onTestFailure(tr);
        Log.info("---------------------------------------------------------");
        Log.error("[TEST IS FAILURED] - " + tr);
        Log.info("---------------------------------------------------------");
        Browser.getInstance().screenshot();
    }
    @Override
    public void onTestSuccess(ITestResult tr) {
        super.onTestSuccess(tr);
        Log.info("[TEST COMPLETED SUCCESSFULLY " + tr.getName() + "]");
        Log.info("---------------------------------------------------------");
    }
    @Override
    public void onTestStart(ITestResult result) {
        Log.info("---------------------------------------------------------");
        Log.info("[TEST START " + result.getName() + "]");
    }
    @Override
    public void onTestSkipped(ITestResult tr) {
        super.onTestFailure(tr);
        Log.info("---------------------------------------------------------");
        Log.error("[TEST IS SKIPPED] - " + tr.getName());
        Log.info("---------------------------------------------------------");
        Browser.getInstance().screenshot();
    }
}

