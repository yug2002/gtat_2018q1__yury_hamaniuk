package com.epam.tat.framework.ui;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Select extends Element {

    public Select(By locator) {
        super(locator);
    }

    public void setOptions(String value){
        waitForAppear();
        WebElement selectWebElement = Browser.getInstance()
                .getWrappedDriver()
                .findElement(locator);
        new org.openqa.selenium.support.ui
                .Select(selectWebElement)
                .selectByValue(value);
    }


}
