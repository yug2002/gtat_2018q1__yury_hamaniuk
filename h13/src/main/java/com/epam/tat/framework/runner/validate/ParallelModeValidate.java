package com.epam.tat.framework.runner.validate;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;
import com.epam.tat.framework.runner.Parameters;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class ParallelModeValidate implements IParameterValidator {

    private static List<String> namesOfMode = Arrays.asList(Parameters.METHODS, Parameters.TESTS, Parameters.CLASSES);
    @Override
    public void validate(String s, String s1) throws ParameterException {
        Optional<String> modes = namesOfMode
                .stream()
                .filter(p->p.equals(s1))
                .findFirst();
        if(modes.equals(Optional.empty())){
            throw new ParameterException("Parameter " + s + " should contain currect name (methods, tests or classes) found " + s1);
        }
    }
}
