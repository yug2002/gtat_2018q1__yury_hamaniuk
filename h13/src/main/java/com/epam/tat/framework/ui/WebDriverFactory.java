package com.epam.tat.framework.ui;

import com.epam.tat.framework.listeners.WebDriverListener;
import com.epam.tat.framework.runner.Parameters;
import com.epam.tat.product.mailru.common.GlobalParameters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class WebDriverFactory {
    public static WebDriver getWebDriver(){
        WebDriver webDriver;
        DesiredCapabilities desiredCapabilities;
        switch (Parameters.instance().getBrowserType()){
            case CHROME:
                desiredCapabilities = DesiredCapabilities.chrome();
                break;
            default:
                throw new RuntimeException("No support for: " + Parameters.instance().getBrowserType());
        }
        try {
            webDriver = new RemoteWebDriver(new URL(GlobalParameters.REMOTE_WEBDRIVER_URL), desiredCapabilities);
        } catch (MalformedURLException e) {
            throw new RuntimeException("Failed to run WebDriver");
        }

        EventFiringWebDriver eventFiringWebDriver = new EventFiringWebDriver(webDriver);
        eventFiringWebDriver.register(new WebDriverListener());
        return eventFiringWebDriver;
    }
}
