package com.epam.tat.framework.listeners;

import com.epam.tat.framework.logging.Log;
import com.epam.tat.framework.runner.Parameters;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.xml.XmlSuite;

public class SuiteListener implements ISuiteListener {
    private XmlSuite.ParallelMode parallelMode;
    private Integer threadCount;
    @Override
    public void onStart(ISuite iSuite) {
        parallelMode = Parameters.instance().getParallelMode()!= null
                ? Parameters.instance().getParallelMode()
                : XmlSuite.ParallelMode.NONE ;
        threadCount = Parameters.instance().getThreadCount() != null
                ? Parameters.instance().getThreadCount()
                : 1;

            iSuite.getXmlSuite().setParallel(parallelMode);
            iSuite.getXmlSuite().setThreadCount(threadCount);

        Log.info("[START SUITE] ParallelMode:" + parallelMode + " threadCount:" + threadCount +" "+ iSuite.getName());
    }

    @Override
    public void onFinish(ISuite iSuite) {
        Log.info("[STOP SUITE] " + iSuite.getName());
    }
}
