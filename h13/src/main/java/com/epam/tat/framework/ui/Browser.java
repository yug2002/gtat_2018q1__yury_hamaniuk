package com.epam.tat.framework.ui;

import com.epam.tat.framework.logging.Log;
import com.epam.tat.framework.runner.CommonTestRuntimeException;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Browser implements WrapsDriver {

    private static ThreadLocal<Browser> instance = new ThreadLocal<Browser>();
    private WebDriver wrappedWebDriver;
    private final static int ELEMENT_VISIBILITY_TIMEOUT_SECOND = 10;
    private Actions actions;

    private Browser(){
        this.wrappedWebDriver = WebDriverFactory.getWebDriver();
        actions = new Actions(this.wrappedWebDriver);
    }
    @Override
    public WebDriver getWrappedDriver() {
        return wrappedWebDriver;
    }

    public static Browser getInstance(){
        if(instance.get() == null){
            instance.set(new Browser());
        }
        return instance.get();
    }

    public void navigate(String url){
        wrappedWebDriver.get(url);
    }

    public void uploadFile(File file, By fileInput){
        String pathToFile = file.getAbsolutePath();
        wrappedWebDriver
                .findElement(fileInput)
                .sendKeys(pathToFile);
    }

    public void click(By by){
        for(int i=0; i<3; i++){
            try {
                wrappedWebDriver
                        .findElement(by)
                        .click();
                break;
            }catch (StaleElementReferenceException ex){
                screenshot();
                Log.debug("Stale element reference exception by locator " + by);
            }
        }
    }
    public void type(By by, String typeText){
        waitForAppear(by);
        wrappedWebDriver
                .findElement(by)
                .sendKeys(typeText);
    }
    public void clear(By by){
        waitForAppear(by);
        wrappedWebDriver
                .findElement(by)
                .clear();
    }
    public void waitForAppear(By by, int timeout){
        new WebDriverWait(wrappedWebDriver, timeout)
                .until(ExpectedConditions.visibilityOfElementLocated(by));
    }
    public void waitForAppear(By by){
        waitForAppear(by, ELEMENT_VISIBILITY_TIMEOUT_SECOND);
    }
    public void waitForClickable(By by, int timeout){
        new WebDriverWait(wrappedWebDriver, timeout)
        .until(ExpectedConditions.elementToBeClickable(by));
    }
    public void waitForPresent(By by){
        new WebDriverWait(wrappedWebDriver, ELEMENT_VISIBILITY_TIMEOUT_SECOND)
                .until(ExpectedConditions.presenceOfElementLocated(by));

    }
    public void waitForClickable(By by){
        waitForClickable(by, ELEMENT_VISIBILITY_TIMEOUT_SECOND);
    }

    public String getText(By by){
       // waitForAppear(by);
        return wrappedWebDriver.findElement(by).getText();
    }


    public boolean isVisible(By by){
        try{
            return wrappedWebDriver
                    .findElement(by)
                    .isDisplayed();
        }catch(NoSuchElementException ex){
            Log.debug("No such WebElement by locator " + by);
            return false;
        }catch(StaleElementReferenceException ex){
            Log.debug("Stale element reference exception by locator " + by);
            return false;
        }

    }

    public void switchToFrame(By by){
        WebElement el = wrappedWebDriver.findElement(by);
        new WebDriverWait(wrappedWebDriver, ELEMENT_VISIBILITY_TIMEOUT_SECOND)
                .until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(el));
    }
    public void switchToDefaultContent(){
        wrappedWebDriver.switchTo().defaultContent();
    }

    public String getAttribute(By by, String attribute){
        return wrappedWebDriver.findElement(by).getAttribute(attribute);
    }
    public List<WebElement> findElements(By by){
        return wrappedWebDriver.findElements(by);
    }

    public void stopBrowser(){
        try{
            if(getWrappedDriver()!=null){
                getWrappedDriver().quit();
            }
        }catch (WebDriverException ex){
            ex.printStackTrace();
        }finally {
            instance.set(null);
        }
    }

    public String getCurrentUrl(){
        return instance.get().getWrappedDriver().getCurrentUrl();
    }

    public byte[] screenshot(){
        File screenshotFile = new File("logs/screenshots/" + System.nanoTime() + ".png");
        try{
            byte[] screenshotBytes = ((TakesScreenshot)getInstance().wrappedWebDriver).getScreenshotAs(OutputType.BYTES);
            org.apache.commons.io.FileUtils.writeByteArrayToFile(screenshotFile, screenshotBytes);
            Log.info("<a href='" + screenshotFile.getAbsolutePath()+ "'>"+screenshotFile.getName()+"</a>");
            return screenshotBytes;

        }catch(IOException ex){
            throw new CommonTestRuntimeException("Failed to write screenshot: ", ex);
        }
    }

    public void actionContextClick(By by){
        actions
                .contextClick(wrappedWebDriver.findElement(by))
                .build()
                .perform();
    }
    public void actionClick(By by){
        actions
                .click(wrappedWebDriver.findElement(by))
                .build()
                .perform();
    }

    public void actionDragNDrop(By drag, By drop){
        actions
                .dragAndDrop(wrappedWebDriver.findElement(drag), wrappedWebDriver.findElement(drop))
                .build()
                .perform();
    }





}
