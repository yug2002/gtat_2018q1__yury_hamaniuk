package com.epam.tat.framework.runner;

import org.testng.TestNG;

import java.util.Arrays;
import java.util.List;

public class RestAssuredRunner {

    public static void main(String[] args){
        TestNG testNG = new TestNG();
        List<String> filesXml = Arrays.asList("./src/main/resources/suites/restAssuredSuite.xml");
        testNG.setTestSuites(filesXml);
        testNG.run();

    }
}
