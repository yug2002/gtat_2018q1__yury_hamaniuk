package com.epam.tat.framework.listeners;

import com.epam.tat.framework.logging.Log;
import com.epam.tat.framework.ui.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;

public class WebDriverListener extends AbstractWebDriverEventListener {
    public static WebDriverListener create(){return new WebDriverListener();}
    @Override
    public void afterNavigateTo(String url, WebDriver driver) {
        Browser.getInstance().screenshot();
        Log.debug("WebDriver navigate to url: " + url );
    }
    @Override
    public void beforeFindBy(By by, WebElement element, WebDriver driver) {
        Log.debug("Befor finding WebElement by locator " + by);
    }

    @Override
    public void beforeClickOn(WebElement element, WebDriver driver) {
        Browser.getInstance().screenshot();
        Log.debug("Befor click on " + element);
    }

}
