package com.epam.tat.test.api;

import com.epam.tat.product.mailru.common.GlobalParameters;
import io.restassured.RestAssured;
import org.testng.annotations.BeforeClass;

public class ApiBaseTest {
    @BeforeClass
    public static void setUp(){
        RestAssured.baseURI = GlobalParameters.DEFAULT_API_URL;
    }
}
