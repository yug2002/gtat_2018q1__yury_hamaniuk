package com.epam.tat.test.api;

import com.epam.tat.product.mailru.common.GlobalParameters;
import com.epam.tat.test.api.forecast.GlobalWeatherParameters;
import com.epam.tat.test.api.forecast.WeatherJsonObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

public class EndPointTest extends ApiBaseTest {

    //requests
    private final String requestLocality = "weather?lat={lat}&lon={lon}&APPID={key}";
    private final String requestAccessKey = "forecast?id=" + GlobalWeatherParameters.ID_MADRID + "&APPID={key}";


    @Test(description = "check that the server response contains an error when entering an incorrect access-key")
    public void checkThatServerResponseContainsErrorWhenEnteringIncorrectAccess(){
        when()
                .get(requestAccessKey, GlobalParameters.EMPTY)
                .then()
                .body("message", equalTo(GlobalParameters.ERROR_INVALID_API_KEY));

    }
    @Test(description = "check that the server response contains the current weather for Gomel")
    public void checkThatServerResponseContainsCurrentWeatherForGomel(){
        when()
                .get(requestLocality, GlobalWeatherParameters.LAT_GOMEL, GlobalWeatherParameters.LON_GOMEL, GlobalParameters.DEFAULT_API_KEY)
                .then()
                .rootPath("weather")
                .body("[0].main",not("Clouds"))
                .rootPath("coord")
                .body("lon",equalTo(GlobalWeatherParameters.LON_GOMEL))
                .and()
                .body("lat", equalTo(GlobalWeatherParameters.LAT_GOMEL))
                .rootPath("sys")
                .body("country", equalTo("BY"));
    }
    @Test(description = "check that the server response contains current name of locality with current coordinates")
    public void checkThatServerResponseContainsCurrentNameOfLocalityWithCurrentCoordinates(){
        when()
                .get(requestLocality, GlobalWeatherParameters.LAT_FEREYDUN_KENAR, GlobalWeatherParameters.LON_FEREYDUN_KENAR, GlobalParameters.DEFAULT_API_KEY)
                .then()
                .body("name", equalTo(GlobalWeatherParameters.FEREYDUN_KENAR));
    }

    @Test(description = "check that the humidity in Madrid exceeds the humidity limit in the next 5 days")
    public void checkThatHumidityExceedsHumidityLimitInNext5Days(){

        WeatherJsonObject weatherJsonObject = new WeatherJsonObject();
        weatherJsonObject.setRequestLocalityForecast(GlobalWeatherParameters.ID_MADRID);

        Assert.assertEquals(weatherJsonObject.isResultHumidityCorrect(), true, "humidity is not more than 89%");
    }

}
