package com.epam.tat.test.api.forecast;

//import groovy.json.internal.ArrayUtils;
import lombok.Getter;

import java.util.SortedSet;
import java.util.TreeSet;

public class Weather {

    @Getter
    private SortedSet<Float> humidities = new TreeSet<>();


    public boolean isMaxHumidityMoreThanLimit(){
        return humidities.last() > GlobalWeatherParameters.LIMIT_OF_HUMIDITY;
    }






}
