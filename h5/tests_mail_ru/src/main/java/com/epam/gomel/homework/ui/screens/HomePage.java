package com.epam.gomel.homework.ui.screens;

import com.epam.gomel.homework.ui.screens.data.DataCheck;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.DataProvider;

import java.util.concurrent.TimeUnit;

public class HomePage extends PageObject {

    @FindBy(css = "form#auth input[type='submit']")
    private WebElement enter;
    @FindBy(xpath="//input[@name='login']")
    private WebElement loginField;
    @FindBy(css = "input[name='password']")
    private WebElement passwordField;
    private String currentUrl;

    public HomePage(WebDriver webDriver) {

        super(webDriver);
        currentUrl = "https://mail.ru/";
    }


    public String getCurrentUrl() {
        return currentUrl;
    }

    public HomePage open(){

        webDriver.get(currentUrl);
        return this;
    }
    public HomePage typeLogin(String login){
        loginField.sendKeys(login);
        return this;
    }

    public HomePage typePassword(String password){
        passwordField.sendKeys(password);
        return this;
    }

    public LoginPage click(){
      enter.click();
      return new LoginPage(webDriver);
    };
}
