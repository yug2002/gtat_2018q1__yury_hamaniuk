package com.epam.gomel.homework.ui.screens;

import com.epam.gomel.homework.ui.GlobalParameters;
import com.sun.xml.internal.ws.util.ByteArrayBuffer;
import lombok.Getter;
import org.openqa.selenium.*;

import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import javax.xml.bind.SchemaOutputResolver;
import java.util.List;


public class LoginPage extends PageObject {
    private final String locatorMyEmail = "//*[@id='PH_user-email']";
    private final String locatorButtonNewLetter = "//*[@id='b-toolbar__left']//a";
    private final String locatorInputField = "div.b-input_textarea>textarea[data-original-name='To']";
    private final String locatorTopicField = "input.b-input[name='Subject']";
    private final String locatorTextField = "//*[@id='tinymce']";
    private final String locatorSendButton = "//span[@class='b-toolbar__btn__text']/../..";
    private final String locatorLinkInBox = "//div[not(@class='message-sent__title')]/a[@href='/messages/inbox/'][not(@class='b-dropdown__list__item')]";
    private final String locatorLinkOutBox = "//div/a[@href='/messages/sent/']";
    private final String locatorLinkLetterIn="div.b-datalist__item:nth-child(1) > div > a";
    private final String locatorAddressesOut ="//div[contains(@class, 'b-datalist_letters_to')]//a//div[@class='b-datalist__item__addr']";
    private final String locatorTopicOut = "//div[contains(@class, 'b-datalist_letters_to')]//a//div[@class='b-datalist__item__subj']";
    private final String locatorDraft = "//div/a[@href='/messages/drafts/']";
    private final String locatorDeleteDraft = "//*[@id='b-toolbar__right']//*[@class='b-toolbar__group']//*[@class='b-toolbar__item']//div[contains(@class, 'b-toolbar__btn_grouped_last')]/i[contains(@class, 'ico_toolbar_remove')]/../..";
    private final String locatorTrash = "//div[contains(@class,'js-href')]/a[@href='/messages/trash/']";

    @Getter
    @FindBy(xpath=locatorMyEmail)
    private WebElement myEmail;

    @FindBy(xpath = locatorButtonNewLetter)
    private WebElement buttonNewLetter;

    @FindBy(css = locatorInputField)
    private WebElement inputField;

    @FindBy(css=locatorTopicField)
    private WebElement topicField;

    @FindBy(xpath=locatorTextField)
    private WebElement textField;

    //@FindBy(css="div[data-name='send'][data-title]")
    @FindBy(xpath=locatorSendButton)
    private WebElement sendButton;
    //@Getter
    @FindBy(xpath = locatorLinkInBox)
    private WebElement linkInBox;

    @FindBy(xpath = locatorLinkOutBox)
    private WebElement linkOutBox;

    @Getter
    @FindBy(css = locatorLinkLetterIn)
    private WebElement linkLetterIn;

    //@Getter
    @FindAll(@FindBy(xpath = locatorAddressesOut))
    private List<WebElement> addressesOut;
    @Getter
    @FindAll(@FindBy(xpath = locatorTopicOut ))
    private List<WebElement> topicsOut;

    @FindBy(xpath =locatorDraft )
    private WebElement draft;

    @FindBy(xpath = locatorDeleteDraft)
    private WebElement deleteDraft;

    @FindBy(xpath=locatorTrash)
    private WebElement trash;

    private HomePage homePage;

    public LoginPage(WebDriver webDriver) {
        super(webDriver);
    }
    public WebElement getLinkInBox(){
        return webDriver.findElement(By.xpath(locatorLinkInBox));
    }

    public List<WebElement> getAddressesOut(){
        WebDriverWait w = new WebDriverWait(webDriver, 60);
        w.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locatorAddressesOut)));
        return webDriver.findElements(By.xpath(locatorAddressesOut));
    }

    public LoginPage open(){
        homePage = new HomePage(webDriver);
        return homePage
            .open()
            .typeLogin(GlobalParameters.DEFAULT_LOGIN)
            .typePassword(GlobalParameters.DEFAULT_PASSWORD)
            .click();
    }
    public LoginPage clickButtonNewLetter(){
        WebDriverWait w = new WebDriverWait(webDriver, 20);
        w.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locatorButtonNewLetter)));
        buttonNewLetter.click();
        return this;
    }
    public LoginPage typeAddressInput(String to){
        inputField.sendKeys(to);
        return this;
    }
    public String getTextMyEmail(){
        return myEmail.getText();
    }
    public LoginPage typeTopic(String topic){
        topicField.sendKeys(topic);
        return this;
    }

    public LoginPage sendButtonClick(){
        sendButton.click();
        return this;
    }


    public LoginPage typeText(String text){
        WebElement frame = webDriver.findElement(By.cssSelector("tbody>tr>td>iframe"));
        (new WebDriverWait(webDriver, 30))
                .until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frame));
        textField.sendKeys(text);
        webDriver.switchTo().defaultContent();
        return this;
    }
    public LoginPage clicklinkIn() throws InterruptedException {

        WebDriverWait w = new WebDriverWait(webDriver, 60);
        w.until(ExpectedConditions.elementToBeClickable(By.xpath(locatorLinkInBox)));
        linkInBox = webDriver.findElement(By.xpath(locatorLinkInBox));
        linkInBox.click();
        return this;
    }

    public LoginPage clicklinkOut() throws InterruptedException {

        WebDriverWait w = new WebDriverWait(webDriver, 40);
        w.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/a[@href='/messages/sent/']")));
        linkOutBox = webDriver.findElement(By.xpath(locatorLinkOutBox));
        linkOutBox.click();
        return this;
    }

    public LoginPage clickDraft() throws InterruptedException {

        assertAndVerifyElement(By.xpath(locatorDraft));
        WebDriverWait w = new WebDriverWait(webDriver, 60);
        w.until(ExpectedConditions.elementToBeClickable(By.xpath(locatorDraft)));
        draft = webDriver.findElement(By.xpath(locatorDraft));
        draft.click();
        return this;
    }

    public LoginPage cklickTrash()throws InterruptedException{
        assertAndVerifyElement(By.xpath(locatorTrash));
        WebDriverWait w = new WebDriverWait(webDriver, 40);
        w.until(ExpectedConditions.elementToBeClickable(By.xpath(locatorTrash)));
        trash = webDriver.findElement(By.xpath(locatorTrash));
        trash.click();
        return this;
    }

    public LoginPage clickDeleteDraft() throws  InterruptedException{
        assertAndVerifyElement(By.xpath(locatorDeleteDraft));
        deleteDraft.click();

        return this;
    }

    public void assertAndVerifyElement(By element) throws InterruptedException {
        boolean isPresent = false;

        for (int i = 0; i < 10; i++) {
            try {
                if (webDriver.findElement(element) != null) {
                    isPresent = true;
                    break;
                }
            } catch (Exception e) {
                // System.out.println(e.getLocalizedMessage());
                Thread.sleep(1000);
            }
        }
        Assert.assertTrue(isPresent, "\"" + element + "\" is not present.");
    }
}
