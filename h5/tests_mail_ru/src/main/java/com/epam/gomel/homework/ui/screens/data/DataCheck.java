package com.epam.gomel.homework.ui.screens.data;

import com.epam.gomel.homework.ui.GlobalParameters;

public class DataCheck {

    public Object[][] dataNegative (){
        return new Object[][]{
            {GlobalParameters.DEFAULT_LOGIN,""},
            {"",GlobalParameters.DEFAULT_PASSWORD},
            {"",""},
            {"asdaasdfasa",GlobalParameters.DEFAULT_PASSWORD},
            {GlobalParameters.DEFAULT_LOGIN,"asdasdasd"},
            {"hgjghj", "DSFGDHF"},
            {"121212212", "43667456"}
        };
    }
}
