package com.epam.gomel.homework.ui.tests;


import com.epam.gomel.homework.ui.GlobalParameters;
import com.epam.gomel.homework.ui.screens.HomePage;
import com.epam.gomel.homework.ui.screens.LoginPage;
import com.epam.gomel.homework.ui.screens.data.DataCheck;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

public class MailRuLoginTest extends MainTest {

    private HomePage homePage;
    private LoginPage loginPage;
    private DataCheck data;


    @Override
    @BeforeClass(alwaysRun = true)
    public void setUp() {
        homePage = new HomePage(webDriver);
        homePage.open();
        data = new DataCheck();
    }
    @AfterMethod
    @Override
    public void back(){
        if(homePage.getCurrentUrl().equals(webDriver.getCurrentUrl()))
        {
            webDriver.navigate().refresh();
        }
        else
            webDriver.navigate().back();
    }
    @Test(description = "Open mail.ru and positive check login page", groups = "login")
    public void checkLoginPositive(){

        loginPage = homePage.typeLogin(GlobalParameters.DEFAULT_LOGIN)
                .typePassword(GlobalParameters.DEFAULT_PASSWORD)
                .click();

        Assert.assertEquals(loginPage.getTextMyEmail(), GlobalParameters.DEFAULT_EMAIL, "logged in");
    }
    @DataProvider(name = "data for negative")
    public Object[][] dataNegative(){
        return data.dataNegative();
    }
    @Test(description = "Open mail.ru and negative check login page", dataProvider = "data for negative", groups = "login")
    public void checkLoginNegative(String login, String password){

        loginPage = homePage.typeLogin(login)
                .typePassword(password)
                .click();

        Assert.assertEquals(loginPage.getTextMyEmail(), "", "not logged in");
    }

    @AfterClass
    @Override
    public void killDriver() {
       webDriver.close();
    }



}
