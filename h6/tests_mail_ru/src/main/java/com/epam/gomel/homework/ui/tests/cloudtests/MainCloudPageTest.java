package com.epam.gomel.homework.ui.tests.cloudtests;

import com.epam.gomel.homework.ui.GlobalParameters;
import com.epam.gomel.homework.ui.screens.cloud_mail.LoginCloudPage;
import com.epam.gomel.homework.ui.screens.cloud_mail.MainCloudPage;
import com.epam.gomel.homework.ui.tests.MainTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;

import java.util.List;
import java.util.Set;


public class MainCloudPageTest extends MainTest {

    private MainCloudPage mainCloudPage;
    private final String locatorHomeCloud = "#navmenu a[href='/home/']";
    private final String relativePathFile = "src/main/resources/web_driver_drawing_cat.gif";
    private Actions action;
    private int delayAppearanceFile = 1000;
    private String currDescriptor ="";

    @BeforeClass(alwaysRun = true)
    @Override
    public void setUp() {

        try {
            mainCloudPage = new LoginCloudPage(webDriver)
                    .open()
                    .clickAuthLink()
                    .typeLogin(GlobalParameters.DEFAULT_LOGIN)
                    .typePassword(GlobalParameters.DEFAULT_PASSWORD)
                    .clickInputButton();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        action = new Actions(webDriver);
        currDescriptor = webDriver.getWindowHandle();
    }
    @Test(description = "create new folder", groups = "createContent", priority = 0)
    public void checkCreationFolder() throws InterruptedException {
        mainCloudPage.closeAdvertising();
        createFolder();
        boolean result = false;
//
        Thread.sleep(delayAppearanceFile);
        List<WebElement> divs = (new WebDriverWait(webDriver, 40))
                .until(new ExpectedCondition<List<WebElement>>() {
                       public List<WebElement> apply(WebDriver driver) {

                           return mainCloudPage.getFolderNameDivs();
                       }
                   }
                );

        if(divs.size()>0){
            for(WebElement div :divs){
                WebElement d = div.findElement(By.cssSelector(".b-filename__text>span.b-filename__name"));
                String text = d.getText();
                if(text.equals(GlobalParameters.DEFAULT_folderName)){
                    result = true;
                    break;
                }
            }
        }
        Assert.assertEquals(result, true, "the folder is missing");
    }
    @Test(description = "check remove folder", priority = 4, groups = "delete")
    public void checkDeletionFolder() throws InterruptedException {
        mainCloudPage.closeAdvertising();
        boolean resultDel = false;
            mainCloudPage
                    //.clickChekBoxDelFolder()
                    .clickButtonDelFolder()
                    .clickPopupButtonDel()
                    .clickPopupSendInTrashButton()
                    .clickClearTrashButton()
                    .clickConfirmClearTrashButton();
            Thread.sleep(delayAppearanceFile);

            List<WebElement> divs = mainCloudPage.getContainsTrasDivs();
            int size = divs.size();
            if(size == 0){
                resultDel = true;
            }
        Assert.assertEquals(resultDel, true, "the folder is not deleted");
    }

    @Test(description = "uploading file", groups = "createContent", priority = 1)
    public void uploadingFile() throws InterruptedException {
        mainCloudPage.closeAdvertising();
        boolean isPresent = false;
        File file = new File(relativePathFile);
        String absolutPath = file.getAbsolutePath();
        mainCloudPage
                .clickUploadButton()
                .typePathFile(absolutPath);

        Thread.sleep(delayAppearanceFile);
        List<WebElement> divs =  mainCloudPage.getFolderNameDivs();
        for(WebElement div : divs){
            String n = div.findElement(By.xpath("..")).getAttribute("data-id");
            if(n.contains(GlobalParameters.DEFAULT_FILE_NAME)){
                isPresent = true;
                break;
            }
        }
        Assert.assertEquals(isPresent, true, "file not found");

    }

    @Test(description = "check drag and drop uploaded file", priority = 2, groups = "dragAndDrop")
    public void checkDragNDrop() throws InterruptedException {
        mainCloudPage.closeAdvertising();
        WebElement draggable = elementContainsFileOrFolderName(GlobalParameters.DEFAULT_FILE_NAME);
        WebElement droppable = elementContainsFileOrFolderName(GlobalParameters.DEFAULT_folderName);
        action
                .dragAndDrop(draggable, droppable)
                .build()
                .perform();
        action
                .click(mainCloudPage.getMoveButton())
                .build()
                .perform();
        action
                .click(droppable)
                .build()
                .perform();

        WebElement element = elementContainsFileOrFolderName(GlobalParameters.DEFAULT_FILE_NAME);
        Assert.assertNotNull(element,"file is not exists");
    }

    @Test(description = "Sharing link to desired element", priority = 3, groups="share")
    public void checkSharing() throws InterruptedException {
        mainCloudPage.closeAdvertising();
        Set<String> olddescriptors = webDriver.getWindowHandles();
        boolean result = false;
        Thread.sleep(delayAppearanceFile);

        WebElement sharing = elementContainsFileOrFolderName(GlobalParameters.DEFAULT_folderName);
        action.contextClick(sharing).build().perform();
        WebElement getLink = mainCloudPage.getGetLink();
        action.click(getLink).build().perform();
        WebElement buttonShare = mainCloudPage.getButtonShare();
        String textLink = mainCloudPage.getInputTextLink().getAttribute("value");
        action.click(buttonShare).build().perform();

        String newWindowHandle = (new WebDriverWait(webDriver, 40))
                .until(new ExpectedCondition<String>() {
                       public String apply(WebDriver driver) {
                           Set<String> newWindowsSet = driver.getWindowHandles();
                           newWindowsSet.removeAll(olddescriptors);
                           return newWindowsSet.size() > 0 ?
                                   newWindowsSet.iterator().next() : null;
                       }
                   }
                );
        /*close popup window*/

        String locatorCssPopupWindowClose = ".b-layer__container.b-layer__container_publish button[data-name='close']";

        WebElement el = webDriver.findElement(By.cssSelector(locatorCssPopupWindowClose));
        action.click(el).build().perform();
        /*********************************/
        webDriver.switchTo().window(newWindowHandle);

        WebDriverWait w = new WebDriverWait(webDriver, 40);
        w.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("iframe[id]")));
        WebElement frame = webDriver.findElement(By.cssSelector("iframe[id]"));
        webDriver.switchTo().frame(frame);
        WebElement body = webDriver.findElement(By.cssSelector("#tinymce"));
        String textFind = body.getText();
        if(textFind.contains(textLink)){
            result = true;
        }
        webDriver.switchTo().defaultContent();

        Assert.assertEquals(result, true,"link is not shared");

    }


    @AfterClass
    @Override
    public void killDriver() {
        webDriver.quit();
    }

    @AfterMethod(alwaysRun = true)
    @Override
    public void back()  {
        webDriver.switchTo().window(currDescriptor);
        WebDriverWait w = new WebDriverWait(webDriver, 20);
        w.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(locatorHomeCloud)));
        WebElement home = webDriver.findElement(By.cssSelector(locatorHomeCloud));
        action.click(home).build().perform();
    }
    private WebElement elementContainsFileOrFolderName(String name){
        List<WebElement> divs =  mainCloudPage.getFolderNameDivs();
        for(WebElement div:divs){
            String dataIdValue = div.getAttribute("data-id");
            if(dataIdValue.contains(name)){
                return div;
            }
        }
        return null;
    }
    private MainCloudPage createFolder() throws InterruptedException {
        return mainCloudPage
                .clickCreateButton()
                .clickCreateFolder()
                .typeFolderName(GlobalParameters.DEFAULT_folderName)
                .clickSubmitCreateFolder();
    }
}
