package com.epam.gomel.homework.ui.tests;


import com.epam.gomel.homework.ui.GlobalParameters;
import com.epam.gomel.homework.ui.screens.login_mail.HomePage;
import com.epam.gomel.homework.ui.screens.login_mail.LoginPage;
import com.epam.gomel.homework.ui.screens.data.DataCheck;
import org.testng.Assert;
import org.testng.annotations.*;

public class MailRuLoginTest extends MainTest {

    private HomePage homePage;
    private LoginPage loginPage;
    private DataCheck data;


    @Override
    @BeforeClass(alwaysRun = true)
    public void setUp() {
        homePage = new HomePage(webDriver);
        homePage.open();
    }
    @AfterMethod
    @Override
    public void back(){
        if(homePage.getCurrentUrl().equals(webDriver.getCurrentUrl()))
        {
            webDriver.navigate().refresh();
        }
        else
            webDriver.navigate().back();
    }
    @Test(description = "Open mail.ru and positive check login page", priority = 1)
    public void checkLoginPositive(){

        loginPage = homePage.typeLogin(GlobalParameters.DEFAULT_LOGIN)
                .typePassword(GlobalParameters.DEFAULT_PASSWORD)
                .click();

        Assert.assertEquals(loginPage.getTextMyEmail(), GlobalParameters.DEFAULT_EMAIL, "logged in");
    }
    @DataProvider(name = "data for negative")
    public Object[][] dataNegative(){
        return DataCheck.dataNegative();
    }
    @Test(description = "Open mail.ru and negative check login page", dataProvider = "data for negative", priority = 0)
    public void checkLoginNegative(String login, String password){

        loginPage = homePage.typeLogin(login)
                .typePassword(password)
                .click();

        Assert.assertEquals(loginPage.getTextMyEmail(), "", "not logged in");
    }

    @AfterClass
    @Override
    public void killDriver() {
       webDriver.close();
    }



}
