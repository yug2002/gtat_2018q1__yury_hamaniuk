package com.epam.gomel.homework.ui.tests;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.util.concurrent.TimeUnit;

public abstract class MainTest {

    protected WebDriver webDriver;

    public MainTest(){
        File driver = new File("./src/main/resources/webdriver/chromedriver.exe");
        String path = driver.getAbsolutePath();
        System.setProperty("webdriver.chrome.driver",path);
        webDriver = new ChromeDriver();
        webDriver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        webDriver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
    }
    @BeforeClass
    public abstract void setUp();
    @AfterClass
    public abstract void killDriver();
    @AfterMethod
    public abstract void back() throws InterruptedException;
}
