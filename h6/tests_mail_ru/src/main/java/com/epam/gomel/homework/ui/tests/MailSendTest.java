package com.epam.gomel.homework.ui.tests;

import com.epam.gomel.homework.ui.GlobalParameters;
import com.epam.gomel.homework.ui.screens.login_mail.LoginPage;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Date;
import java.util.List;

public class MailSendTest extends MainTest {

    private LoginPage loginPage;
    private Actions action;

    @BeforeClass(alwaysRun = true)
    @Override
    public void setUp() {
        loginPage = new LoginPage(webDriver);
        action = new Actions(webDriver);
        loginPage.open();
    }

    @Test(description = "sending and receiving a letter", priority = 2)
    public void checkSendLetter()  {

        loginPage.clickButtonNewLetter()
            .typeAddressInput(GlobalParameters.DEFAULT_EMAIL)
            .typeTopic(GlobalParameters.DEFAULT_TOPIC)
            .typeText(GlobalParameters.DEFAULT_TEXT)
            .sendButtonClick()
            .clicklinkOut();

        boolean flagIn = false;
        boolean flagOut = false;
        String addr = "";
        String top = "";

        String topicTextletter = GlobalParameters.DEFAULT_TOPIC.concat(GlobalParameters.DEFAULT_TEXT);

        int count = loginPage.getAddressesOut().size();

        for(int i=0; i< count; i++){
            addr = loginPage.getAddressesOut().get(i).getText();
            top = loginPage.getTopicsOut().get(i).getText();
            if(addr.equals(GlobalParameters.DEFAULT_EMAIL) && top.contains(topicTextletter)){
                flagOut = true;
                break;
            }
        }
        loginPage.clicklinkIn();
        WebElement linkLetter = loginPage.getLinkLetterIn();

        String attrAddr = linkLetter.getAttribute("title");

        WebElement addressIn = linkLetter.findElement(By.xpath("//div[@class='b-datalist__item__subj']"));
        String topicTextIn = addressIn.getText();

        if(flagOut && topicTextIn.equals(top) && attrAddr.contains(GlobalParameters.DEFAULT_EMAIL)){
            flagIn = true;
        }

        Assert.assertEquals(flagIn, true, "letter is not sent and received");
    }
    @Test(description = "Verify that mail is not sent without an address", priority = 10)
    public void checkSendMailWithoutAddress() {

        loginPage.clickButtonNewLetter()
                 .typeTopic(GlobalParameters.DEFAULT_TOPIC)
                 .typeText(GlobalParameters.DEFAULT_TEXT)
                 .sendButtonClick();

        Assert.assertEquals(isAlertPresent(), true, "alert is not present");

    }
    @Test(description = "Verify that mail is sent without a topic and body ", priority = 3)
    public void checkSendMailWithoutTopicBody(){

        WebElement inBox = loginPage.getLinkInBox().findElement(By.xpath("span[1]"));
        int countIn1 = Integer.parseInt(inBox.getText());
        loginPage.clickButtonNewLetter()
                 .typeAddressInput(GlobalParameters.DEFAULT_EMAIL)
                 .sendButtonClick();

        String locatorConfirm = "div#MailRuConfirm div.is-compose-empty_in>form>div.popup__controls>button.confirm-ok";
        WebDriverWait w = new WebDriverWait(webDriver, 15);
        w.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(locatorConfirm)));
        WebElement confirmButton = webDriver.findElement(By.cssSelector(locatorConfirm));

        confirmButton.click();

        webDriver.navigate().refresh();

        String span = webDriver.findElement(By.xpath("//div[not(@class='message-sent__title')]/a[@href='/messages/inbox/'][not(@class='b-dropdown__list__item')]/span[1]")).getText();

        int countIn2 = Integer.parseInt(span);

        loginPage.clicklinkIn();
        WebElement linkLetter = loginPage.getLinkLetterIn();
        WebElement topicBody = linkLetter.findElement(By.xpath("//div[@class='b-datalist__item__subj']"));

        String topicBodyIn = topicBody.getText();

        mouseMove();

        boolean flagInOutTopic = false;

        loginPage.clicklinkOut();
        String topicBodyOut="";
        for (WebElement t:loginPage.getTopicsOut()){
            topicBodyOut = t.getText();
            if(topicBodyOut.equals(topicBodyIn)){
                flagInOutTopic = true;
                break;
            }
        }

        boolean res = ((countIn2-countIn1)==1)&flagInOutTopic;

        Assert.assertEquals(res, true, "empty mail is not sent");

    }

    @Test(description = "Check the complete removal of the draft in the trash" ,priority = 4)
    public void checkTrash(){
        Date date = new Date();
        String curTime = String.valueOf(date.getTime());
        loginPage.clickButtonNewLetter()
                .typeText(GlobalParameters.DEFAULT_TEXT + curTime);

        action.keyDown(Keys.CONTROL).sendKeys("s").keyUp(Keys.CONTROL).perform();

        loginPage.clickDraft();
        isAlertPresent();
        String strLocatorMain = "//div[contains(@class,'b-datalist_letters_to')]/div[@class='b-datalist__body']/div";
        List<WebElement> divTextBodies = webDriver.findElements(By.xpath(strLocatorMain));
        String text ="";


        boolean isPresent = false;
        for (WebElement textBody:
                divTextBodies) {

            WebElement subj = textBody.findElement(By.cssSelector(".b-datalist__item__subj"));
            text = subj.getText();
            if(text.contains(GlobalParameters.DEFAULT_TEXT + curTime + GlobalParameters.DEFAULT_SIGN)){

                WebElement cbx = subj.findElement(By.xpath("../../../../div[1]"));
                cbx.click();
                loginPage.clickDeleteDraft();
                isPresent = true;
                break;
            }
        }
        loginPage.cklickTrash();

        String locatorSubj = "//*[@id='b-letters']/*[@class='b-datalists']/div/div[contains(@class,'b-datalist_letters_from')]//*[@class='b-datalist__item js-datalist-item']//*[@class='b-datalist__item__subj']";
        List<WebElement> subjDraftInTrash = webDriver.findElements(By.xpath(locatorSubj));
        String textDraftInTrash="";

        for(WebElement el: subjDraftInTrash){
            textDraftInTrash =  el.getText();
            if(isPresent&&textDraftInTrash.contains(GlobalParameters.DEFAULT_TEXT + curTime + GlobalParameters.DEFAULT_SIGN)){
                WebElement cbx = el.findElement(By.xpath("../../../../div[1]"));
                cbx.click();
                List<WebElement> dels = webDriver.findElements(By.xpath("//*[@id='b-toolbar__right']//*[@class='b-toolbar__group']//*" +
                        "[@class='b-toolbar__item']//div[contains(@class, 'b-toolbar__btn_grouped_first')]/i[contains(@class, 'ico_toolbar_remove')]/../.."));
                for (WebElement del:dels) {
                    if(del.isDisplayed()){
                        del.click();
                    }
                }
                break;
            }
        }

        boolean ok = searchingElementInTrash(locatorSubj, 0, curTime);

        Assert.assertEquals(ok, true,"draft is not delete" );

    }
    @AfterClass
    @Override
    public void killDriver() {
       webDriver.close();
    }

    @AfterMethod
    @Override
    public void back() {
        webDriver.navigate().back();
        isAlertPresent();
    }

    private boolean isAlertPresent(){
        try{
            webDriver.switchTo().alert().accept();
            return true;
        }
        catch(NoAlertPresentException ex){
            return false;
        }
    }

    private void mouseMove(){
        Actions action = new Actions(webDriver);
        action.moveToElement(loginPage.getMyEmail()).build().perform();
    }

    private boolean searchingElementInTrash(String xpathLocator, int numberElement, String curTime){
        try{
            List<WebElement> subjDraftInTrashCheck = webDriver.findElements(By.xpath(xpathLocator));
            String textDraftInTrash = subjDraftInTrashCheck.get(numberElement).getText();
            if(subjDraftInTrashCheck.get(numberElement).isDisplayed()){
                if(subjDraftInTrashCheck.size()<=numberElement|subjDraftInTrashCheck.size()==0){
                    return true;
                }
                else if(textDraftInTrash.contains(curTime)){
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                searchingElementInTrash(xpathLocator, numberElement+1, curTime);
            }

        }catch(StaleElementReferenceException ex){
            searchingElementInTrash(xpathLocator, numberElement, curTime);
        }
        return searchingElementInTrash(xpathLocator, numberElement+1, curTime);
    }


}
