package com.epam.gomel.homework.ui.screens.cloud_mail;

import com.epam.gomel.homework.ui.GlobalParameters;
import com.epam.gomel.homework.ui.screens.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class MainCloudPage extends PageObject {

    private final int delayHihlight = 1000;
    private final String locatorCssCreateButton ="#toolbar-left .b-dropdown";
    private final String locatorCssFolderLink =".b-dropdown_expanded>div>a[data-name='folder']";
    private final String locatorCssInputFolderName = "form.layer__form input";
    private final String locatorCssSubmitCreateFolder = "form.layer__form button.btn_main";
    private final String locatorCssDivNameFolder =".b-collection__item_axis-y>.b-thumb";
    private final String locatorXpathCheckBoxDelFolder = "//*[text()='"
            + GlobalParameters.DEFAULT_folderName
            +"' and @class='b-filename__name']/../../../..//*[@class='b-checkbox__box']";
    private final String locatorXpathDelFolder = "//*[contains(@class,'b-toolbar__btn_remove') and not(contains(@class,'b-toolbar__btn_disabled'))]";
    private final String localCssPopupButtonDel = ".layer_remove .b-layer__controls__buttons>button.btn_main";
    private final String localXpathPopupSendInTrashButton = "//*[@class='layer__footer']/button[not(contains(@class, 'btn_main'))]";
   // private final String locatorCssCheckBoxInTrash = ".datalist-item__col_checkbox";
    private final String locatorXpathClearTrashButton ="//div[contains(@class,'b-toolbar__btn_clear') and not(contains(@class,'b-toolbar__btn_disabled'))]";
    private final String locatorCssConfirmClearTrashButton = ".layer_remove .b-layer__controls button[data-name='empty']";
    private final String locatorCssContainsTrash = ".datalist-item__content_datalist-mode-trashbin .b-filename_icon .b-filename__name";
    private final String locatorCssInputFile = ".layer_upload__controls input[type='file']";
    private final String locatorXpathUploadButton = "//*[@id='cloud_toolbars']//*[contains(@class, 'ico_toolbar_upload')]/..";
    private final String locatorCssMoveButton = "button[data-name='move']";
    private final String locatorCssFileInFolder =".b-collection__container_datalist-mode-thumb>div>div>.b-thumb_image";
    //share
    private final String locatorCssGetLink = "a[data-name='publish']";
    private final String locatorCssButtonShare="button[data-id='email']";
    private final String locatorCssInputTextLink = "input[data-name='url']";
    private final String locatorCssAdvertising =".b-layer__container_disko-promo";

    @FindBy(css=locatorCssCreateButton)
    private WebElement createButton;

    @FindBy(css = locatorCssFolderLink)
    private WebElement createFolder;

    @FindBy(css = locatorCssInputFolderName)
    private WebElement inputFolderName;

    @FindBy(css = locatorCssSubmitCreateFolder)
    private WebElement submitCreateFolder;

    @FindAll(@FindBy(css = locatorCssDivNameFolder))
    private List<WebElement> folderNameDivs;

    @FindBy(xpath = locatorXpathCheckBoxDelFolder)
    private WebElement checkBoxDelFolder;

    @FindBy(xpath=locatorXpathDelFolder)
    private WebElement buttonDelFolder;

    @FindBy(css = localCssPopupButtonDel)
    private WebElement popupButtonDel;

    @FindBy(xpath = localXpathPopupSendInTrashButton)
    private WebElement popupSendInTrashButton;

//    @FindBy(css = locatorCssCheckBoxInTrash)
//    private WebElement checkBoxInTrash;

    @FindBy(xpath = locatorXpathClearTrashButton)
    private WebElement clearTrashButton;

    @FindBy(css = locatorCssConfirmClearTrashButton)
    private WebElement confirmClearTrashButton;

    @FindAll(@FindBy (css = locatorCssContainsTrash))
    List<WebElement> containsTrasDivs;

    @FindBy(css = locatorCssInputFile)
    private WebElement inputFile;

    @FindBy(xpath = locatorXpathUploadButton)
    private WebElement uploadButton;

    @FindBy(css=locatorCssMoveButton)
    private WebElement moveButton;

    @FindAll(@FindBy(css = locatorCssFileInFolder))
    private List<WebElement> filesInFolder;

    @FindBy(css=locatorCssGetLink)
    private WebElement getLink;

    @FindBy(css=locatorCssButtonShare)
    private WebElement buttonShare;

    @FindBy(css=locatorCssInputTextLink)
    private WebElement inputTextLink;


    public MainCloudPage(WebDriver webDriver) {
        super(webDriver);
    }


    public MainCloudPage clickCreateButton() throws InterruptedException {
        highlightElementAndClick(createButton);
        return this;
    }
    public MainCloudPage clickCreateFolder() throws InterruptedException {
        highlightElementAndClick(createFolder);
        //createFolder.click();
        return this;
    }

    public MainCloudPage typeFolderName(String folderName){
        inputFolderName.sendKeys(folderName);
        return this;
    }
    public MainCloudPage clickSubmitCreateFolder() throws InterruptedException {
        highlightElementAndClick(submitCreateFolder);
       // submitCreateFolder.click();
        return this;
    }

    public List<WebElement> getFolderNameDivs(){
        WebDriverWait w = new WebDriverWait(webDriver, 20);
        w.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(locatorCssDivNameFolder)));
        folderNameDivs = webDriver.findElements(By.cssSelector(locatorCssDivNameFolder));
        return folderNameDivs;
    }

    public MainCloudPage clickChekBoxDelFolder() throws InterruptedException {
        WebDriverWait w = new WebDriverWait(webDriver, 40);
        w.until(ExpectedConditions.elementToBeClickable(By.xpath(locatorXpathCheckBoxDelFolder)));
        checkBoxDelFolder = webDriver.findElement(By.xpath(locatorXpathCheckBoxDelFolder));
        //WebElement w = checkBoxDelFolder.findElement(By.xpath(".."));
        checkBoxDelFolder.click();
        //highlightElementAndClick(w);
        //checkBoxDelFolder.click();
        return this;
    }

    public MainCloudPage clickButtonDelFolder() throws InterruptedException {
        WebDriverWait w = new WebDriverWait(webDriver, 20);
        w.until(ExpectedConditions.elementToBeClickable(By.xpath(locatorXpathDelFolder)));
        highlightElementAndClick(buttonDelFolder);
        //buttonDelFolder.click();
        return this;
    }

    public MainCloudPage clickPopupButtonDel() throws InterruptedException {

        highlightElementAndClick(popupButtonDel);
       // popupButtonDel.click();
        return this;
    }

    public MainCloudPage clickPopupSendInTrashButton() throws InterruptedException {
        WebDriverWait w = new WebDriverWait(webDriver, 20);
        w.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(localXpathPopupSendInTrashButton)));
        highlightElementAndClick(popupSendInTrashButton);
        //popupSendInTrashButton.click();
        return this;
    }

//    public MainCloudPage clickCheckBoxInTrash() throws InterruptedException {
//        WebDriverWait w = new WebDriverWait(webDriver, 20);
//        w.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(locatorCssCheckBoxInTrash)));
//        highlightElementAndClick(checkBoxInTrash);
//       // checkBoxInTrash.click();
//        return this;
//    }

    public MainCloudPage clickClearTrashButton(){
        WebDriverWait w = new WebDriverWait(webDriver, 20);
        w.until(ExpectedConditions.elementToBeClickable(By.xpath(locatorXpathClearTrashButton)));
        clearTrashButton.click();
        return this;
    }

    public MainCloudPage clickConfirmClearTrashButton(){
        confirmClearTrashButton.click();
        return this;
    }

    public List<WebElement> getContainsTrasDivs(){
        return containsTrasDivs;
    }

    public MainCloudPage typePathFile(String absolutePath){
        inputFile.sendKeys(absolutePath);
        return this;
    }

    public MainCloudPage clickUploadButton(){
        uploadButton.click();
        return this;
    }

    public WebElement getMoveButton(){
        WebDriverWait w = new WebDriverWait(webDriver, 20);
        w.until(ExpectedConditions.elementToBeClickable(By.cssSelector(locatorCssMoveButton)));
        moveButton = webDriver.findElement(By.cssSelector(locatorCssMoveButton));
        return moveButton;
    }

    public List<WebElement> getFilesInFolder(){
        WebDriverWait w = new WebDriverWait(webDriver, 20);
        w.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(locatorCssFileInFolder)));
        filesInFolder = webDriver.findElements(By.cssSelector(locatorCssFileInFolder));
        return filesInFolder;
    }
    public WebElement getGetLink(){
        getLink = webDriver.findElement(By.cssSelector(locatorCssGetLink));
        return getLink;
    }

    public WebElement getButtonShare(){
        WebDriverWait w = new WebDriverWait(webDriver, 20);
        w.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(locatorCssButtonShare)));
        buttonShare = webDriver.findElement(By.cssSelector(locatorCssButtonShare));
        return buttonShare;
    }
    public WebElement getInputTextLink(){
        inputTextLink = webDriver.findElement(By.cssSelector(locatorCssInputTextLink));
        return inputTextLink;
    }
    private void highlightElementAndClick(WebElement element) throws InterruptedException {

        ((JavascriptExecutor) webDriver).executeScript("arguments[0].style.border ='5px solid red'", element);
        Thread.sleep(delayHihlight);
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].style.border ='0'", element);
        WebDriverWait w = new WebDriverWait(webDriver, 40);
        w.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
        //((JavascriptExecutor)webDriver).executeScript("arguments[0].style.border='0'", element);
    }
    public void closeAdvertising() {
        try{
            WebDriverWait w = new WebDriverWait(webDriver, 5);
            w.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(locatorCssAdvertising)));
            List<WebElement> advertisings = webDriver.findElements(By.cssSelector(locatorCssAdvertising));
            if(advertisings.size()!=0){
                WebElement cl = webDriver.findElement(By.cssSelector(".b-panel__close__icon"));
                if(cl.isDisplayed())
                    cl.click();
            }
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }

    }
 }
