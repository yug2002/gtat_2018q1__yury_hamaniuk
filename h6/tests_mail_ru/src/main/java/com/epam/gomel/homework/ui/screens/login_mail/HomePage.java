package com.epam.gomel.homework.ui.screens.login_mail;

import com.epam.gomel.homework.ui.screens.cloud_mail.LoginCloudPage;
import com.epam.gomel.homework.ui.screens.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends PageObject {

    private final String locatorCssEnter = "form#auth input[type='submit']";
    private final String locatorXpathLoginField = "//input[@name='login']";
    private final String locatorCssPasswordField = "input[name='password']";
    private final String locatorCssCloud ="a.mailbox__icon_cloud";
    @FindBy(css = locatorCssEnter)
    private WebElement enter;
    @FindBy(xpath=locatorXpathLoginField)
    private WebElement loginField;
    @FindBy(css = locatorCssPasswordField)
    private WebElement passwordField;
    @FindBy(css = locatorCssCloud)
    private WebElement cloud;

    private String currentUrl;

    public HomePage(WebDriver webDriver) {

        super(webDriver);
        currentUrl = "https://mail.ru/";
    }


    public String getCurrentUrl() {
        return currentUrl;
    }

    public HomePage open(){

        webDriver.get(currentUrl);
        return this;
    }
    public HomePage typeLogin(String login){
        loginField.sendKeys(login);
        return this;
    }

    public HomePage typePassword(String password){
        WebDriverWait w = new WebDriverWait(webDriver, 60);
        w.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(locatorCssPasswordField)));
        passwordField = webDriver.findElement(By.cssSelector(locatorCssPasswordField));
        passwordField.sendKeys(password);
        return this;
    }

    public LoginPage click(){
      enter.click();
      return new LoginPage(webDriver);
    };
    public LoginCloudPage clickCloud(){

        cloud.click();
        return new LoginCloudPage(webDriver);
    }
}
