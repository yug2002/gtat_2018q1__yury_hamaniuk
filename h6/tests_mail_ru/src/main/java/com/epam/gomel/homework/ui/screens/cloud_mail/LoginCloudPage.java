package com.epam.gomel.homework.ui.screens.cloud_mail;

import com.epam.gomel.homework.ui.screens.login_mail.HomePage;
import com.epam.gomel.homework.ui.screens.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginCloudPage extends PageObject {

    private final String urlLoginCloud = "https://cloud.mail.ru/";

    private final String locatorCssAuthLink = "a#PH_authLink";
    private final String locatorCssLoginField = "input[name='Login']";
    private final String locatorCssPasswordField = "input#ph_password";
    private final String locatorCssInputButton = "input[type='submit']";
    private final String locatorCssCheckLogin = "#PH_user-email";
    private final int delayHihlight = 1000;

    private HomePage homePage;

    @FindBy(css=locatorCssAuthLink)
    private WebElement authLink;

    @FindBy(css=locatorCssLoginField)
    private WebElement loginField;

    @FindBy(css=locatorCssPasswordField)
    private WebElement passwordField;

    @FindBy(css = locatorCssInputButton)
    private WebElement inputButton;

    @FindBy(css = locatorCssCheckLogin)
    private WebElement checkLoginLink;

    public LoginCloudPage(WebDriver webDriver) {
        super(webDriver);

    }

    public String getUrlLoginCloud(){
        return urlLoginCloud;
    }

    public LoginCloudPage open(){
        webDriver.get(urlLoginCloud);
        return this;
    }

    public LoginCloudPage clickAuthLink() throws InterruptedException {
        highlightElementAndClick(authLink);
       // authLink.click();
        return this;
    }

    public LoginCloudPage typeLogin(String login){
        WebDriverWait w = new WebDriverWait(webDriver, 20);
        w.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(locatorCssLoginField)));
        //loginField.clear();
        loginField.sendKeys(login);
        return this;
    }

    public LoginCloudPage typePassword(String password){
        passwordField.sendKeys(password);
        return this;
    }
    public MainCloudPage clickInputButton() throws InterruptedException {
        WebElement el = inputButton.findElement(By.xpath(".."));
        //inputButton = webDriver.findElement(By.cssSelector(locatorCssInputButton));
        highlightElementAndClick(el);
         return new MainCloudPage(webDriver);
    }

    public WebElement getCheckLoginLink(){
        WebDriverWait w = new WebDriverWait(webDriver, 60);
        w.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(locatorCssCheckLogin)));
        checkLoginLink = webDriver.findElement(By.cssSelector(locatorCssCheckLogin));
        return checkLoginLink;

    }
    private void highlightElementAndClick(WebElement element) throws InterruptedException {

        ((JavascriptExecutor)webDriver).executeScript("arguments[0].style.border ='5px solid red'", element);
        Thread.sleep(delayHihlight);
        ((JavascriptExecutor)webDriver).executeScript("arguments[0].style.border ='0'", element);
        element.click();
        //((JavascriptExecutor)webDriver).executeScript("arguments[0].style.border='0'", element);

    }

}
