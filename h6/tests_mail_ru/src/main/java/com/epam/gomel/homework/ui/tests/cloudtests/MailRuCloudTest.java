package com.epam.gomel.homework.ui.tests.cloudtests;

import com.epam.gomel.homework.ui.GlobalParameters;
import com.epam.gomel.homework.ui.screens.cloud_mail.LoginCloudPage;
import com.epam.gomel.homework.ui.screens.data.DataCheck;
import com.epam.gomel.homework.ui.tests.MainTest;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.*;

public class MailRuCloudTest extends MainTest {

    private LoginCloudPage loginPageCloud;


    @BeforeClass(alwaysRun = true)
    @Override
    public void setUp() {
        loginPageCloud = (new LoginCloudPage(webDriver)).open();
    }

    @Test(description = "positive check login to cloud", groups = "positive")
    public void checkLoginCloudPositive() throws InterruptedException {
        loginPageCloud.clickAuthLink()
                .typeLogin(GlobalParameters.DEFAULT_LOGIN)
                .typePassword(GlobalParameters.DEFAULT_PASSWORD)
                .clickInputButton();
        String result = loginPageCloud.getCheckLoginLink().getText();

        Assert.assertEquals(result, GlobalParameters.DEFAULT_EMAIL, "login failed");
    }
    @DataProvider(name = "data for negative")
    public Object[][] dataNegative(){ return DataCheck.dataNegative();}
    @Test(description = "negative check login to cloud", dataProvider = "data for negative", groups = "negative")
    public void checkLoginCloudNegative(String login, String password) throws InterruptedException {

            WebElement findReg = null;
            boolean flagReg = false;
            loginPageCloud.clickAuthLink()
                    .typeLogin(login)
                    .typePassword(password)
                    .clickInputButton();
            try{
                findReg = webDriver.findElement(By.id("#PH_user-email"));
                if(!findReg.isDisplayed()){
                    flagReg = true;
                }
            }catch (NoSuchElementException ex){
                flagReg = true;
            }
            Assert.assertEquals(flagReg, true, "login is successfull");

    }



    @AfterClass
    @Override
    public void killDriver() {
       webDriver.quit();
    }

    @BeforeMethod(alwaysRun = true)
    public void refresh(){
        webDriver.navigate().to(loginPageCloud.getUrlLoginCloud());
    }

    @AfterMethod
    @Override
    public void back() throws InterruptedException {
        Thread.sleep(200);

    }
}
