package com.epam.tat.product.mailru.letter.service;

import com.epam.tat.framework.logging.Log;
import com.epam.tat.framework.ui.Browser;
import com.epam.tat.product.mailru.common.GlobalParameters;
import com.epam.tat.product.mailru.letter.bo.Letter;
import com.epam.tat.product.mailru.letter.exception.MailruLetterException;
import com.epam.tat.product.mailru.letter.screen.ProfilePage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.Date;
import java.util.List;

public class LetterService {
    private ProfilePage profilePage = new ProfilePage();
    private String alertMessage="";
    private String time = null;

    public boolean send(Letter letter) {
        Log.debug("SEND LETTER");
        wright(letter)
                .clickSendButton();
        if(letter.getText().equals(GlobalParameters.EMPTY)){
            profilePage.clickConfirmButtonEmptyLetter();
        }
        checkSendSuccesfull();
        return checkLetterFoundInIncomigMailAndInOutgoingMail();
    }

    public boolean isFoundLetterInIncomingAndOutgoingMail() {

        String topicOut = null;
        String addressOut = null;
        String topicIn = null;
        String addressIn = null;
        profilePage
                .clickLinkIncomingBox();
        profilePage.clickLinkIncomingLetter();

        topicIn = profilePage.getTextTopicOpenedIncomingLetter();
        addressIn = profilePage.getTextAddressOpenedIncomingLetter();

        profilePage.clickLinkOutBox();
        List<WebElement> listOfLinksOutgoingMail = profilePage.getListOfWebElementOfOutgoingMail();
        if(listOfLinksOutgoingMail.size()>0){
            for(WebElement a: listOfLinksOutgoingMail){
                a.click();
                topicOut = profilePage.getTextTopicOpenedIncomingLetter();
                addressOut = profilePage.getTextWhoSentLetterInOpenedOutgoingLetter();
                if(topicOut.equals(topicIn)&&addressOut.equals(addressIn)){
                    return true;
                }
                else{
                    profilePage.back();
                }
            }
        }else{
            throw new MailruLetterException("Error: listOfLinksOutgoingMail not found");
        }

        return false;
    }

    private ProfilePage wright(Letter letter){
        Log.debug("[WRIGHT LETTER]");
        profilePage
                .clickButtonNewLetter();
        isAlertPresent();
        return profilePage
                .typeAddressInputField(letter.getTo())
                .typeTopicField(letter.getTopic())
                .typeTextLetter(letter.getText());
    }

    private void saveDraft(){
        Log.debug("[SAVE DRAFT]");
        Actions actions = new Actions(Browser.getInstance().getWrappedDriver());
        actions.keyDown(Keys.CONTROL).sendKeys("s").keyUp(Keys.CONTROL).build().perform();
    }

    public String createDraft(Letter letter){
        Log.debug("CREATE DRAFT");
        time = String.valueOf(new Date().getTime());
        String newTextLetter = letter.getText() + time;

        letter.setText(newTextLetter);
        wright(letter);
        saveDraft();

        profilePage
                .clickLinkDraft();

        isAlertPresentCancel();
        saveDraft();

        profilePage
                .clickLinkLetterInDrafts();

        String text = profilePage.getTextLetter();
        if(text.contains(time)){

            return text;
        }
        else{
            throw new MailruLetterException("Error: in drafts is not such letter");
        }

    }

    public boolean deleteDraft(){
        Log.debug("DELETE DRAFT");
        boolean result = false;
        profilePage
                .clickLinkDraft()
                .clickLinkLetterInDrafts();

        String text = profilePage.getTextLetter();

        if(text.contains(time)){
            profilePage.back();
            profilePage.clickCheckboxInDraft()
                    .clickDeleteDraft();
        }
        else{
            throw new MailruLetterException("Error: there is no such draft here");
        }
        profilePage.clickLinkTrash();
        boolean isContainsText = profilePage.isLetterContainsText(time);

        if(isContainsText){
            profilePage.clickLinkTrash();
            profilePage.clickCheckboxLetterInTrash();
            profilePage.clickButtonDeleteLetterFromTrash();
        }

        isContainsText = profilePage.isLetterContainsText(time);
        if(!isContainsText){
            result = true;
        }

        return result;

    }

    private void checkSendSuccesfull(){
        if(isAlertPresent()){
            throw new MailruLetterException("error: " + alertMessage);
        }
    }

    private boolean checkLetterFoundInIncomigMailAndInOutgoingMail() {
        Log.debug("[CHECK THAT LETTER IS FOUND IN INCOMING AND OUTGOING MAIL]");
        if(!isFoundLetterInIncomingAndOutgoingMail()){
            throw new MailruLetterException("Error: letter is not found in Imcoming mail or Outgoing mail");
        }
        return true;
    }


    private boolean isAlertPresent(){
        try{
            alertMessage = Browser
                    .getInstance()
                    .getWrappedDriver()
                    .switchTo()
                    .alert()
                    .getText();
            Browser
                    .getInstance()
                    .getWrappedDriver()
                    .switchTo()
                    .alert()
                    .accept();
            return true;
        }
        catch(NoAlertPresentException ex){
            return false;
        }
    }
    private boolean isAlertPresentCancel(){
        try{
              Browser
                     .getInstance()
                     .getWrappedDriver()
                     .switchTo()
                     .alert()
                     .dismiss();

            return true;
        }
        catch(NoAlertPresentException ex){
            return false;
        }
    }

}
