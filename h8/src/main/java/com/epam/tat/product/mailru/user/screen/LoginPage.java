package com.epam.tat.product.mailru.user.screen;

import com.epam.tat.framework.logging.Log;
import com.epam.tat.framework.ui.Browser;
import com.epam.tat.framework.ui.Element;
import com.epam.tat.product.mailru.common.screen.AbstractLoginPage;
import org.openqa.selenium.By;

import java.util.concurrent.TimeUnit;

public class LoginPage extends AbstractLoginPage {

    private static final By loginInput = By.xpath("//input[@name='login']");
    private static final By passwordInput = By.cssSelector("input[name='password']");
    private static final By loginButton = By.cssSelector("input.o-control");
    private static final By errorMessage = By.cssSelector("div[id='mailbox:error']");

    private Element loginfield = new Element(loginInput);
    private Element passwordfield = new Element(passwordInput);
    private Element submit = new Element(loginButton);
    private Element errorfield = new Element(errorMessage);
    @Override
    public LoginPage open(){
        Browser.getInstance().getWrappedDriver().manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        Browser.getInstance().getWrappedDriver().manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);
        return this;
    }
    @Override
    public LoginPage typeLogin(String login){

        loginfield.clear();
        loginfield.type(login);
        Log.debug("type login - " + login);
        return this;
    }
    @Override
    public LoginPage typePassword(String password){
        passwordfield.clear();
        passwordfield.type(password);
        Log.debug("type password - " + password);
        return this;
    }
    @Override
    public void clickInputButton(){
        submit.click();
    }

    public boolean hasErrorMessage(){
        //errorfield.waitForAppear();
        return errorfield.isVisible();
    }
    @Override
    public String getErrorMessage(){
        if(hasErrorMessage())
            return errorfield.getText();
        return null;
    }

}
