package com.epam.tat.product.mailru.user.exception;

import com.epam.tat.framework.runner.CommonTestRuntimeException;

public class MailruUserException extends CommonTestRuntimeException {

    public MailruUserException(String message) {
        super(message);
    }
}
