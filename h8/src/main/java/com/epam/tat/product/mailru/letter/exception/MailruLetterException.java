package com.epam.tat.product.mailru.letter.exception;

import com.epam.tat.framework.runner.CommonTestRuntimeException;

public class MailruLetterException extends CommonTestRuntimeException {

    public MailruLetterException(String message) {
        super(message);
    }
}
