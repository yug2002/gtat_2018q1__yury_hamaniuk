package com.epam.tat.product.mailru.cloud.folder.screen;

import com.epam.tat.framework.logging.Log;
import com.epam.tat.framework.ui.Element;
import com.epam.tat.product.mailru.cloud.folder.screen.extra.ProfileCloudStoreOfElements;
import com.epam.tat.product.mailru.cloud.folder.screen.extra.ProfileCloudStoreOfLocators;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;

import java.io.File;

public class ProfileCloudPage {

    private ProfileCloudStoreOfElements elements = new ProfileCloudStoreOfElements();
    private ProfileCloudStoreOfLocators locators = new ProfileCloudStoreOfLocators();


    public ProfileCloudPage clickCreateButton(){
        elements.getCreateButton().click();
        return this;
    }

    public ProfileCloudPage clickCreateFolder(){
        elements.getFolderLink().click();
        return this;
    }

    public ProfileCloudPage typeFolderName(String folderName){
        elements.getInputFolderName().type(folderName);
        return this;
    }

    public ProfileCloudPage clickSubmitCreateFolder(){
        elements.getSubmitCreateFolder().click();
        return this;
    }
    public boolean isPresentMyFolder(){
        try{
            elements.getNameFolder().waitForAppear();
        }catch (TimeoutException ex){
            return false;
        }
        boolean res = elements.getNameFolder().isVisible();
        return res;
    }
    public ProfileCloudPage clickCheckboxOfFolder(){
        elements.getCheckboxDelFolder().click();
        return this;
    }

    public ProfileCloudPage clickButtonDelFolder(){
        elements.getDelFolder().waitForAppear();
        elements.getDelFolder().click();
        return this;
    }

    public ProfileCloudPage clickButtonOfConfirmOfDeleting(){
        elements.getPopupButtonDel().waitForAppear();
        elements.getPopupButtonDel().click();
        return this;
    }

    public ProfileCloudPage clickGoToTrash(){
        elements.getPopupSendInTrashButton().waitForAppear();
        elements.getPopupSendInTrashButton().click();
        return this;
    }

    public ProfileCloudPage closeAdvertising(){
        try{
            elements.getAdvertising().waitForAppear(10);
            elements.getAdvertising().click();
        }catch (Exception ex){
            Log.debug("[advertising is absent]");
        }
        return this;
    }

    public ProfileCloudPage clearTrash(){
        elements.getClearTrashButton().waitForAppear();
        elements.getClearTrashButton().click();
        return this;

    }

    public ProfileCloudPage clickConfimClearTrash(){
        elements.getConfirmClearTrashButton().waitForAppear();
        elements.getConfirmClearTrashButton().click();
        return this;
    }
    public ProfileCloudPage clickUploadButton(){
        elements.getUploadButton().click();
        return this;
    }

    public ProfileCloudPage uploadFile(File file){
        elements.getInputFile().uploading(file);
        return this;
    }

    public boolean isPresenceUploadedFile(){
        elements.getUploadedFile().waitForAppear();
        boolean res = elements.getUploadedFile().isVisible();
        return res;
    }
    public ProfileCloudPage clickHomeLink(){
        elements.getCloudHome().click();
        return this;
    }
    public WebElement getDraggableWebElementWithDefaultFileName(){
        WebElement draggable = new Element(locators.getLocatorXpathDraggableFile()).getWebElement();
        return draggable;
    }

    public WebElement getDroppableWebElementWithDefaultFolderName(){

        elements.getDroppable().waitForAppear();
        WebElement element = elements.getDroppable().getWebElement();
        return element;
    }

    public WebElement getMoveButton(){
        elements.getMoveButton().isPresent();
        WebElement moveFile = (elements.getMoveButton()).getWebElement();
        return moveFile;
    }
    public boolean isMyFolderContainsFile(){
        elements.getContainsFileInMyFolder().waitForAppear();
        return elements.getContainsFileInMyFolder().isVisible();
    }

    public WebElement getGetLink() {
       // elements.getLinkToShare().waitForAppear();
        return elements.getLinkToShare().getWebElement();
    }

    public String getInputTextLink() {
        elements.getInputTextLink().waitForAppear();
        String link = elements.getInputTextLink().getAttribute("value");
        return link;
    }

    public WebElement buttonShare(){
        elements.getButtonShare().waitForAppear();
        return elements.getButtonShare().getWebElement();
    }

    public WebElement popupWindowLinkToShare(){
        return elements.getPopupWindowLinkToShare().getWebElement();
    }
    public void switcToFrame(){
        elements.getFrame().switchToFrame();
    }

    public void SwitchToDefaultContent(){
        elements.getFrame().switchToDefaultContent();
    }

    public String getTextOfLinkFromSharingLetter(){
       elements.getTextSharingLink().waitForAppear();
       return elements.getTextSharingLink().getText();
    }

}
