package com.epam.tat.product.mailru.cloud.folder.service;

import com.epam.tat.framework.logging.Log;
import com.epam.tat.framework.ui.Browser;
import com.epam.tat.product.mailru.cloud.folder.bo.Folder;
import com.epam.tat.product.mailru.cloud.folder.screen.ProfileCloudPage;
import com.epam.tat.product.mailru.cloud.login.screen.LoginCloudPage;
import com.epam.tat.product.mailru.common.GlobalParameters;
import com.epam.tat.product.mailru.common.screen.AbstractLoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.util.Set;

public class FolderCloudService {

    private ProfileCloudPage profileCloudPage = new ProfileCloudPage();
    private AbstractLoginPage loginCloudPage = new LoginCloudPage();


    public void open(){
        Log.info("[LOGIN]");
        loginCloudPage.open();
        ((LoginCloudPage)loginCloudPage)
                .clickAuthLink()
                .typeLogin(GlobalParameters.DEFAULT_LOGIN)
                .typePassword(GlobalParameters.DEFAULT_PASSWORD)
                .clickInputButton();

    }

    public boolean createFolder(Folder folder){
        Log.info("[CREATE FOLDER]");
        return profileCloudPage
                .closeAdvertising()
                .clickCreateButton()
                .clickCreateFolder()
                .typeFolderName(folder.getName())
                .clickSubmitCreateFolder()
                .isPresentMyFolder();
    }
    public boolean deleteFolder(){
        Log.info("[DELETE FOLDER]");
        boolean isPresent = profileCloudPage.isPresentMyFolder();
        if(isPresent){
            profileCloudPage
//                    .closeAdvertising()
                    .clickCheckboxOfFolder()
                    .clickButtonDelFolder()
                    .clickButtonOfConfirmOfDeleting()
                    .clickGoToTrash()
                    .clearTrash()
                    .clickConfimClearTrash()
                    .isPresentMyFolder();
        }
        isPresent = profileCloudPage.isPresentMyFolder();
        return isPresent;
    }
    public boolean uploadFile(String relativePath){
        Log.info("[UPLOAD FILE]");
        File file = new File(relativePath);
        return profileCloudPage
                .clickUploadButton()
                .uploadFile(file)
                .isPresenceUploadedFile();


    }
    public void atHome(){
        profileCloudPage.clickHomeLink();
    }
    public boolean dragNDrop(Folder folder){

        Actions actions = new Actions(Browser.getInstance().getWrappedDriver());
        createFolder(folder);
        WebElement draggable = profileCloudPage.getDraggableWebElementWithDefaultFileName();
        WebElement droppable = profileCloudPage.getDroppableWebElementWithDefaultFolderName();
        Log.info("[ACTION DRAG'N'DROP]");
        actions
                .dragAndDrop(draggable, droppable)
                .build()
                .perform();
        Log.info("ACTION CLICK MOVEBUTTON");
        actions
                .click(profileCloudPage.getMoveButton())
                .build()
                .perform();
        actions
                .click(droppable)
                .build()
                .perform();


        return isMyFolderContainsFile();

    }
    private Set<String> getOldDescriptors(){
        return Browser.getInstance().getWrappedDriver().getWindowHandles();
    }

    private String getNewWindowHandle(Set<String> olddescriptors){
        String newWindowHandle = (new WebDriverWait(Browser.getInstance().getWrappedDriver(), 40))
            .until(new ExpectedCondition<String>() {
                   public String apply(WebDriver driver) {
                       Set<String> newWindowsSet = driver.getWindowHandles();
                       newWindowsSet.removeAll(olddescriptors);
                       return newWindowsSet.size() > 0 ?
                               newWindowsSet.iterator().next() : null;
           }
                       }
        );
        return newWindowHandle;
    }

    private void switchToHandle(String descriptor){
        Browser.getInstance().getWrappedDriver().switchTo().window(descriptor);
    }
    public boolean shareItem(){
        Log.info("[SHARE]");
        Actions actions = new Actions(Browser.getInstance().getWrappedDriver());
        boolean res = false;

        Set<String> oldDescriptors = getOldDescriptors();

        WebElement sharing = profileCloudPage.getDroppableWebElementWithDefaultFolderName();
        actions.contextClick(sharing).build().perform();

        WebElement getLink = profileCloudPage.getGetLink();
        actions.click(getLink).build().perform();

        WebElement buttonShare = profileCloudPage.buttonShare();
        String textLink = profileCloudPage.getInputTextLink();
        actions.click(buttonShare).build().perform();

        String newWindowHandle = getNewWindowHandle(oldDescriptors);

        WebElement popupWindow = profileCloudPage.popupWindowLinkToShare();
        actions.click(popupWindow).build().perform();

        switchToHandle(newWindowHandle);

        profileCloudPage.switcToFrame();

        String link = profileCloudPage.getTextOfLinkFromSharingLetter();

        if(link.contains(textLink)){
            res = true;
        }
        profileCloudPage.SwitchToDefaultContent();
        switchToHandle(oldDescriptors.iterator().next());
        Log.info("[END SHARE]");
        return res;
    }

    private boolean isMyFolderContainsFile(){
        return profileCloudPage.isMyFolderContainsFile();

    }






}
