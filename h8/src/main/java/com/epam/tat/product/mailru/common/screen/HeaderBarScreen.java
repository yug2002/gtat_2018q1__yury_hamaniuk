package com.epam.tat.product.mailru.common.screen;

import com.epam.tat.framework.logging.Log;
import com.epam.tat.framework.ui.Element;
import com.epam.tat.product.mailru.common.GlobalParameters;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;

public class HeaderBarScreen {

    private static final By registerLink = By.xpath("//*[@id='PH_user-email']");
    private static final By byLogoutLink = By.id("PH_logoutLink");

    private Element register = new Element(registerLink);
    private Element logout = new Element(byLogoutLink);

    public String getTextOfLink(){
        try{
            register.waitForAppear(2);
        }
        catch(TimeoutException ex){
            Log.debug(ex.getMessage());
        }
        if(register.isVisible())
            return register.getText();
        return null;
    }
    public void waitLinkRegistration(){
        register.waitForAppear();
    }

    public boolean isUserLoggedIn(){
        String find = getTextOfLink();
        if(find!=null)
            return find.equals(GlobalParameters.DEFAULT_EMAIL);
        return false;
    }

    public void clickLogout(){

        logout.waitForClickable();
        if(logout.isVisible())
            logout.click();
    }

}
