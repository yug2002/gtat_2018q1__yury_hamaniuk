package com.epam.tat.test;

import com.epam.tat.framework.ui.Browser;
import com.epam.tat.product.mailru.cloud.folder.bo.Folder;
import com.epam.tat.product.mailru.cloud.folder.bo.FolderFactory;
import com.epam.tat.product.mailru.cloud.folder.service.FolderCloudService;
import com.epam.tat.product.mailru.common.GlobalParameters;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ProfileCloudTest {

    private FolderCloudService folderCloudService = new FolderCloudService();
    private Folder defaulFolder = FolderFactory.createDefaultFolder();

    @BeforeClass
    public void setUp(){
        Browser browser = Browser.getInstance();
        browser.navigate(GlobalParameters.DEFAULT_LOGIN_CLOUD_URL);
        folderCloudService.open();
    }
    @AfterMethod
    public void home(){
        folderCloudService.atHome();
    }
    @AfterClass
    public void killDriver(){
        Browser.getInstance().stopBrowser();
    }
    @Test(description = "check that folder is created in cloud profile page", priority = 1)
    public void checkThatFolderIsCreated(){
        boolean res = folderCloudService.createFolder(defaulFolder);
        Assert.assertEquals(res, true, "the folder is not present");
    }
    @Test(description = "verify that folder is removed completely", priority = 2)
    public void checkThatFolderIsRemoved(){
        boolean res = folderCloudService.deleteFolder();
        Assert.assertEquals(res, false, "folder is not deleted");
    }
    @Test(description = "verify that the file is uploaded successfully", priority = 3)
    public void checkThatFileUploadsSuccessfuly(){
        boolean res = folderCloudService.uploadFile(GlobalParameters.RELATIVE_PATH_TO_FILE_TO_UPLOAD);
        Assert.assertEquals(res, true,"file is not uploaded");
    }
    @Test(description = "check drag and drop uploaded file", priority=4)
    public void checkThatFileDragNDropSuccessfuly(){
        boolean res = folderCloudService.dragNDrop(defaulFolder);
        Assert.assertEquals(res, true,"file is absent");
    }
    @Test(description = "verify that the link to the item is shared", priority = 5)
    public void checkThatLinkToItemIsShared(){
        boolean res = folderCloudService.shareItem();
        Assert.assertEquals(res, true,"link is not shared");
    }


}
