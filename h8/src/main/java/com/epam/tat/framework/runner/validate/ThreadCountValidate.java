package com.epam.tat.framework.runner.validate;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

public class ThreadCountValidate implements IParameterValidator {

    @Override
    public void validate(String s, String s1) throws ParameterException {
        if(Integer.valueOf(s1)<=0||s1==null){
            throw new ParameterException("Parameter " + s +" should contain currect data (int>0 or !=null) " +
                    "to the suite (found "+ s1 +")");
        }
    }
}
