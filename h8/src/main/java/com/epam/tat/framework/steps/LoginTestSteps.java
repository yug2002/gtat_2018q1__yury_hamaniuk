package com.epam.tat.framework.steps;

import com.epam.tat.framework.ui.Browser;
import com.epam.tat.product.mailru.common.GlobalParameters;
import com.epam.tat.product.mailru.user.bo.User;
import com.epam.tat.product.mailru.user.bo.UserBuilder;
import com.epam.tat.product.mailru.user.bo.UserFactory;
import com.epam.tat.product.mailru.user.screen.LoginPage;
import com.epam.tat.product.mailru.user.service.UserService;
import org.jbehave.core.annotations.*;
import org.testng.Assert;

public class LoginTestSteps {

    private UserService userService = new UserService(new LoginPage());
    private User defaultUser = UserFactory.createDefaultUser();
    private Browser browser;

    @BeforeScenario(uponType = ScenarioType.ANY)
    public void beforeScenario(){
        browser = Browser.getInstance();
        browser.navigate(GlobalParameters.DEFAULT_PRODUCT_URL);
    }

    @Given("I opened mail.ru main page")
    public void givenIOpendMailruMainPage(){
        Browser.getInstance().getWrappedDriver().manage().window().maximize();
    }
    @When("I log in using existing username and password")
    public void whenILogInUsingExistingUsernameAndPassword(){
        userService.unsafeLogin(defaultUser);
    }

    @Then("I am successfully logged")
    public void thenIAmSuccessfullyLogged(){
        Assert.assertEquals(userService.isLoginSuccessfull(), true, "Login failed ");
    }

    @When("I log in using not existing <login> and <password>")
    public void whenILogInUsingNotExistingLoginAndPassword(@Named("login") String login, @Named("password") String password){
        defaultUser = new UserBuilder()
                .userlogin(login)
                .userpassword(password)
                .build();
        userService.unsafeLogin(defaultUser);
    }

    @Then("I am not logged")
    public void thenIAmNotLogged(){
        Assert.assertEquals(userService.isLoginSuccessfull(), false, "Login is not failed ");
    }
    @AfterScenario(uponType = ScenarioType.ANY)
    public void afterScenario(){
        browser.stopBrowser();
    }
}
