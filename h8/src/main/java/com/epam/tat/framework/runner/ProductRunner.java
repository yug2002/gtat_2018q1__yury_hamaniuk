package com.epam.tat.framework.runner;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.epam.tat.framework.listeners.SuiteListener;
import com.epam.tat.framework.listeners.TestListener;
import com.epam.tat.framework.logging.Log;
import org.apache.log4j.PropertyConfigurator;
import org.testng.TestNG;

import java.io.File;

public class ProductRunner {

    public static TestNG configureTestNG(){

        TestNG testNG = new TestNG();
        testNG.addListener(new TestListener());
        testNG.addListener(new SuiteListener());
        return testNG;
    }


    public static void main(String[] args){
        TestNG testNG = configureTestNG();
        File log4jProperties = new File("./src/main/resources/log4j/log4j.properties");
        PropertyConfigurator.configureAndWatch(log4jProperties.getAbsolutePath());
        Log.info("parse Cli");
        parseCli(args, testNG);
        testNG.run();

    }

    private static void parseCli(String[] args, TestNG testNG){
        Log.info("Parse cli using JCommander");

        JCommander jCommander = new JCommander(Parameters.instance());
        try{

            jCommander.parse(args);
        }catch(ParameterException ex){
            Log.debug(ex.getMessage() + " " + ex);
            System.exit(1);
        }

        if(Parameters.instance().isHelp()){
            jCommander.usage();
            System.exit(0);
        }
        if(Parameters.instance().getSuite()!=null){

            Parameters.instance().suiteFiles.add(Parameters.instance().getSuite());
            testNG.setTestSuites(Parameters.instance().suiteFiles);
        }



    }

}
