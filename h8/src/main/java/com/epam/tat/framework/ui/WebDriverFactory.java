package com.epam.tat.framework.ui;

import com.epam.tat.framework.listeners.WebDriverListener;
import com.epam.tat.framework.runner.Parameters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.io.File;

public class WebDriverFactory {
    public static WebDriver getWebDriver(){
        WebDriver webDriver;
        switch (Parameters.instance().getBrowserType()){
            case CHROME:
                System.setProperty("webdriver.chrome.driver", new File(Parameters.instance().getChromeDriver()).getAbsolutePath());
                webDriver = new ChromeDriver();
                break;
            default:
                throw new RuntimeException("No support for: " + Parameters.instance().getBrowserType());


        }
        EventFiringWebDriver eventFiringWebDriver = new EventFiringWebDriver(webDriver);
        eventFiringWebDriver.register(new WebDriverListener());
        return eventFiringWebDriver;
    }
}
