package com.epam.tat.framework.steps;

import com.epam.tat.product.mailru.common.GlobalParameters;
import com.epam.tat.product.mailru.letter.bo.Letter;
import com.epam.tat.product.mailru.letter.bo.LetterBuilder;
import com.epam.tat.product.mailru.letter.bo.LetterFactory;
import com.epam.tat.product.mailru.letter.exception.MailruLetterException;
import com.epam.tat.product.mailru.letter.service.LetterService;
import com.epam.tat.product.mailru.user.bo.UserFactory;
import com.epam.tat.product.mailru.user.screen.LoginPage;
import com.epam.tat.product.mailru.user.service.UserService;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.testng.Assert;

public class LetterTestSteps {

    private LetterService letterService = new LetterService();
    private Letter defaultLetter;
    private String textLetter = null;
    private boolean result = false;

    @Given("I create default letter with correct data filled")
    public void givenICreateDefaultLetterWithCorrectDataFilled(){

        new UserService(new LoginPage()).login(UserFactory.createDefaultUser());
        defaultLetter = LetterFactory.createDefaultLetter();
    }
    @When("I send and receive letter with correct data filled")
    public void whenISendAndReceiveLetterWithCorrectDataFilled(){
        result = letterService.send(defaultLetter);

    }
    @Then("sending and receiving letters were successful")
    public void thenSendingAndReceivingLettersWereSuccessful(){
        Assert.assertEquals(result, true, "letter did not send");
    }

    @Given("I create default letter with empty address field")
    public void givenICreateDefaultLetterWithEmptyAddressField(){

        new UserService(new LoginPage()).login(UserFactory.createDefaultUser());
        defaultLetter = new LetterBuilder()
                .letterTo(GlobalParameters.EMPTY)
                .letterTopic(GlobalParameters.DEFAULT_TOPIC)
                .letterText(GlobalParameters.DEFAULT_TEXT_LETTER)
                .build();
    }
    @When("I try to send letter without filled-in address")
    public void whenITryToSendLetterWithoutFilledinAddress(){
        result = false;
        try{
            letterService.send(defaultLetter);
        }catch(MailruLetterException ex){
            result = true;
        }
    }
    @Then("I can't to send this letter")
    public void thenICantToSendThisLetter(){
        Assert.assertEquals(result, true, "letter is sent");
    }
    @Given("I create letter without a topic and body")
    public void givenICreateLetterWithoutATopicAndBody(){

        new UserService(new LoginPage()).login(UserFactory.createDefaultUser());
        result = false;
        defaultLetter = new LetterBuilder()
                .letterTo(GlobalParameters.DEFAULT_EMAIL)
                .letterTopic(GlobalParameters.EMPTY)
                .letterText(GlobalParameters.EMPTY)
                .build();
    }

    @When("I try to send letter without a topic and body")
    public void whenITryToSendLetterWithoutATopicAndBody(){
        result = letterService.send(defaultLetter);
    }

    @Then("I send this letter successfully")
    public void thenISendThisLetterSuccessfully(){
        Assert.assertEquals(result, true, "letter did not send");
    }

    @Given("I create letter only with text")
    public void givenICreateLetterOnlyWithText(){
        new UserService(new LoginPage()).login(UserFactory.createDefaultUser());
        result = false;
        defaultLetter = new LetterBuilder()
                .letterTo(GlobalParameters.EMPTY)
                .letterTopic(GlobalParameters.EMPTY)
                .letterText(GlobalParameters.DEFAULT_TEXT_LETTER)
                .build();
    }

    @When("I try to create and delete draft")
    public void whenITryToCreateAndDeleteDraft(){
        textLetter = letterService.createDraft(defaultLetter);
        result = letterService.deleteDraft();

    }

    @Then("draft is created and deleted successfully")
    public void thenDraftIsCreatedAndDeletedSuccessfully(){
        Assert.assertEquals(textLetter!=null & result, true, "draft did not create or delete");
    }

}
