Feature: Login to Mail.ru

Narrative:
As a user
I want to login into mail.ru
So that I must check validation

Scenario: checking successfull login
Given I opened mail.ru main page
When I log in using existing username and password
Then I am successfully logged

Scenario: checking validation with wrong creds
Given I opened mail.ru main page
When I log in using not existing <login> and <password>
Then I am not logged

Examples:
|login|password|
|examplefail1|qwert678|
|examplefail2|qwert678|
|examplefail3|qwert678|
|examplefail4|qwert678|