package MyFactory;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import com.epam.gomel.homework.Mood;
import org.testng.Assert;
import org.testng.annotations.*;


import java.time.Month;

public class BoyTestFactory {

     private Girl girl;


     public BoyTestFactory(Girl girl){
         this.girl = girl;
     }

    @DataProvider(name = "Data for spend create")
    public Object [][] Params(){
        return new Object[][]{
                {new Boy(Month.JULY, 1000.0, girl), 100.0, 900.0 },
                {new Boy(Month.AUGUST, 1000.0, girl), 1000.0, 0.0 },
                {new Boy(Month.SEPTEMBER, 0.0), 0.0, 0.0 }
        };
    }
    @Test(description = "check spendSomeMoney method", dataProvider = "Data for spend create",groups="spend", priority = 5)

    public void spendSomeMoneyTest(Boy boy, double spend, double expectedResult){

        boy.spendSomeMoney(spend);

        Assert.assertEquals(boy.getWealth(), expectedResult, "the welth decreased by the correct amount");
    }

    @DataProvider(name = "Data for BoyExeptions create")
    public Object [][] ParamsExeptions(){
        return new Object[][]{
                {new Boy(Month.JUNE, 1000.0, girl), 2000.0},
                {new Boy(Month.JANUARY, 0.0, girl), 100.0}
        };
    }
    @Test(description = "amountForSpending > Welth",
            expectedExceptions = RuntimeException.class,
            expectedExceptionsMessageRegExp = "^Not enough money.*", dataProvider = "Data for BoyExeptions create",groups = "spend")
    public void spendSomeMoneyExeption(Boy boy, double amountForSpending){
        boy.spendSomeMoney(amountForSpending);
    }
    @DataProvider(name = "Data for Summer create")
    public Object[][] ParamsSummer(){
        return new Object[][]{
                {new Boy(Month.JUNE), true},
                {new Boy(Month.JULY), true},
                {new Boy(Month.AUGUST), true},
                {new Boy(Month.JANUARY), false},
                {new Boy(Month.FEBRUARY), false},
                {new Boy(Month.MARCH), false},
                {new Boy(Month.APRIL), false},
                {new Boy(Month.MAY), false},
                {new Boy(Month.SEPTEMBER), false},
                {new Boy(Month.OCTOBER),false},
                {new Boy(Month.NOVEMBER), false},
                {new Boy(Month.DECEMBER), false}
        };
    }

    @Test(description = "check when the boy was born", dataProvider = "Data for Summer create",groups = "summer")
    public void isSummerMonthCheck(Boy boy, boolean expectedResult){
        boolean isSummer = boy.isSummerMonth();
        Assert.assertEquals(isSummer, expectedResult, "this boy was born in the summer or not");
    }
    @DataProvider(name = "Data for Rich create")
    public Object[][] ParamsIsRich(){
        return new Object[][]{

                {new Boy(Month.MARCH, 1000000.0), true},
                {new Boy(Month.APRIL, 1000001.0), true},
                {new Boy(Month.MAY, 2000000.0), true},
                {new Boy(Month.MARCH, 0.0), false},
                {new Boy(Month.APRIL, 999999.0), false},
                {new Boy(Month.MAY, 500000.0), false},
        };
    }
    @Test(description = "check up that hi is rich", groups = "rich",
            dependsOnMethods = "isSummerMonthCheck", dataProvider = "Data for Rich create")

    public void isRichCheck(Boy boy, boolean expectedResult){

        boolean actual = boy.isRich();
        Assert.assertEquals(actual,expectedResult,"the boy is rich or not");
    }



}

