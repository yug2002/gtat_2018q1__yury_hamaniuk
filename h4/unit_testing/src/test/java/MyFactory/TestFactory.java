package MyFactory;

import com.epam.gomel.homework.Girl;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

public class TestFactory {

    @DataProvider
    public Object[][] Params(){
        return new Object[][]{
                {new Girl(true, true)},
                {new Girl(true, false)},
                {new Girl(false, true)},
                {new Girl(false, false)},
        };
    }
    @Factory(dataProvider = "Params")
    public Object[] createTest(Girl girl){

        return new Object[]{new BoyTestFactory(girl)};
    }
}
