package interfaces;

import com.epam.gomel.homework.Human;
import com.epam.gomel.homework.Mood;

public interface IFriendsTestInterface {
    void SetUp();
    void MoodCheck(Human person, Mood expectedResult);
    void isRichCheck(Human person, boolean expectedResult);
}
