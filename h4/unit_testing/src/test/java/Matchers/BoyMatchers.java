package Matchers;

import com.epam.gomel.homework.Boy;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

public class BoyMatchers {

    public static Matcher<Boy> isRich(){
        return new FeatureMatcher<Boy,Boolean>(is(true),
                "boy should be rich", "rich - "){
            @Override
            protected Boolean featureValueOf(Boy boy) {
                return boy.isRich();
            }
        };
    }

    public static Matcher<Boy> isSimmerMonth(){
        return new FeatureMatcher<Boy,Boolean>(is(true),
                "boy who must was born in summer","was born in summer - ") {
            @Override
            protected Boolean featureValueOf(Boy boy) {
                return boy.isSummerMonth();
            }
        };
    }
    public static Matcher<Boy> isPrettyGirlFriend() {
        return new FeatureMatcher<Boy, Boolean>(is(true),
                "boy who must have pretty girl","have pretty girl - ") {
            @Override
            protected Boolean featureValueOf(Boy boy) {
                return boy.isPrettyGirlFriend();
            }
        };
    }
}
