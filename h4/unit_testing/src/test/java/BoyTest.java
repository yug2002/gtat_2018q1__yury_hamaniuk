import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import com.epam.gomel.homework.Human;
import com.epam.gomel.homework.Mood;
import interfaces.IFriendsTestInterface;
import org.hamcrest.MatcherAssert;
import org.testng.Assert;
import org.testng.annotations.*;
import java.time.Month;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;

import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.*;
import static Matchers.BoyMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@Listeners(TestListener.class)
public class BoyTest implements IFriendsTestInterface {

    private  Girl[] girls;


    @BeforeClass(alwaysRun = true)
    @Override
    public void SetUp(){
        girls = new Girl[]{
            new Girl(true, true),
            new Girl(true, false),
            new Girl(false, true),
            new Girl(false, false)
        };
    }
    @DataProvider(name = "Data for BoyExeptions create")
    public Object [][] ParamsExeptions(){
        return new Object[][]{
            {new Boy(Month.JUNE, 1000.0, girls[0]), 2000.0},
            {new Boy(Month.JANUARY, 0.0, girls[1]), 100.0}
        };
    }
    @DataProvider(name = "Data for Boy create")
    public Object [][] Params(){
        return new Object[][]{
                {new Boy(Month.JULY, 1000.0, girls[0]), 100.0, 900.0 },
                {new Boy(Month.AUGUST, 1000.0, girls[0]), 1000.0, 0.0 },
                {new Boy(Month.SEPTEMBER, 0.0), 0.0, 0.0 }
        };
    }
    @DataProvider(name = "Data for Summer create")
    public Object[][] ParamsSummer(){
        return new Object[][]{
                {new Boy(Month.JUNE), true},
                {new Boy(Month.JULY), true},
                {new Boy(Month.AUGUST), true},
                {new Boy(Month.JANUARY), false},
                {new Boy(Month.FEBRUARY), false},
                {new Boy(Month.MARCH), false},
                {new Boy(Month.APRIL), false},
                {new Boy(Month.MAY), false},
                {new Boy(Month.SEPTEMBER), false},
                {new Boy(Month.OCTOBER),false},
                {new Boy(Month.NOVEMBER), false},
                {new Boy(Month.DECEMBER), false}
        };
    }

    @DataProvider(name = "Data for Rich create")
    public Object[][] ParamsIsRich(){
        return new Object[][]{

                {new Boy(Month.MARCH, 1000000.0), true},
                {new Boy(Month.APRIL, 1000001.0), true},
                {new Boy(Month.MAY, 2000000.0), true},
                {new Boy(Month.MARCH, 0.0), false},
                {new Boy(Month.APRIL, 999999.0), false},
                {new Boy(Month.MAY, 500000.0), false},
        };
    }
    @DataProvider(name = "Data for Pretty create")
    public Object[][] ParamsPretty(){
        return new Object[][]{
                {new Boy(Month.JULY, 50000.0, girls[0]), true},
                {new Boy(Month.JULY, 50000.0, girls[2]), false},
                {new Boy(Month.JULY, 50000.0), false},

        };
    }
    @DataProvider(name = "Data for Mood create")
    public Object[][] ParamsExcellent(){
        return new Object[][]{
                {new Boy(Month.JULY, 1000000.0, girls[0]), Mood.EXCELLENT},
                {new Boy(Month.SEPTEMBER, 1000000.0, girls[0]), Mood.GOOD},
                {new Boy(Month.JULY, 1000000.0), Mood.NEUTRAL},
                {new Boy(Month.SEPTEMBER, 1000000.0), Mood.BAD},
                {new Boy(Month.SEPTEMBER, 10000.0,girls[0] ), Mood.BAD},
                {new Boy(Month.JUNE), Mood.BAD},
                {new Boy(Month.SEPTEMBER), Mood.HORRIBLE},
        };
    }

    @Test(description = "check spendSomeMoney method", dataProvider = "Data for Boy create", groups="spend")
    public void spendSomeMoneyTest(Boy boy, double amountForSpending, double expectedResult){
        boy.spendSomeMoney(amountForSpending);
        Assert.assertEquals(boy.getWealth(), expectedResult, "the welth decreased by the correct amount");
    }
    @Test(description = "amountForSpending > Welth", dataProvider = "Data for BoyExeptions create",
            expectedExceptions = RuntimeException.class,
            expectedExceptionsMessageRegExp = "^Not enough money.*", groups = "spend")
    public void spendSomeMoneyExeption(Boy boy, double amountForSpending){
        boy.spendSomeMoney(amountForSpending);
    }

    @Test(description = "check when the boy was born", dataProvider = "Data for Summer create", groups = "summer")
    public void isSummerMonthCheck(Boy boy, boolean expectedResult){
        boolean isSummer = boy.isSummerMonth();
        Assert.assertEquals(isSummer, expectedResult, "this boy was born in the summer or not");
    }

    @Test(description = "check up that hi is rich", dataProvider = "Data for Rich create", groups = "rich", dependsOnMethods = "isSummerMonthCheck")
    @Override
    public void isRichCheck(Human person, boolean expectedResult){
        Boy b =(Boy) person;
        boolean actual = b.isRich();
        Assert.assertEquals(actual,expectedResult,"the boy is rich or not");
    }
    @Test(description = "check up that girlfriend is pretty", dataProvider = "Data for Pretty create", groups = "pretty")
    public void isPrettyGirlFriendCheck(Boy boy, boolean expectedResult){
        boolean actual = boy.isPrettyGirlFriend();
        Assert.assertEquals(actual,expectedResult,"the girlfriend is pretty?...");
    }
    @Override
    @Test(description = "check up Mood", dataProvider = "Data for Mood create",groups = "mood")
    public void MoodCheck(Human person, Mood expectedResult){
        Mood actual = person.getMood();
        Assert.assertEquals(actual, expectedResult, "Get the mood of boy");
    }

    @DataProvider(name = "Data for hamcrest create")
    public Object[][] ParamsHamcrest(){
        return new Object[][]{
                {new Boy(Month.JULY, 2000000.0, girls[1])},
                {new Boy(Month.JUNE, 4000000.0, girls[1])},
                {new Boy(Month.JULY, 5000000.0,girls[1])},
                {new Boy(Month.AUGUST, 10000000.0,girls[1])},
                {new Boy(Month.JULY, 1000555.0,girls[1])},
        };
    }

    @Test(dataProvider ="Data for Rich create" )
    public void checkIsRichHamcrest(Boy boy, boolean expectedResult){
        assertThat(boy.isRich(), is(expectedResult));

    }

    @Test(description = "verify hamcrest and my matcher",dataProvider ="Data for hamcrest create" )
    public void BoyIsRichIsPrettyGirlFriendIsSummerMonth(Boy boy){
        assertThat(boy, both(isRich()).and(isPrettyGirlFriend()).and(isSimmerMonth()));
    }

}

