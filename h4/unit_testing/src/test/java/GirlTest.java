import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import com.epam.gomel.homework.Human;
import com.epam.gomel.homework.Mood;
import interfaces.IFriendsTestInterface;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.time.Month;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Listeners(TestListener.class)
public class GirlTest implements IFriendsTestInterface {

    private Boy[] boys;
    private Boy mockBoy = mock(Boy.class);

    @Override
    @BeforeClass(alwaysRun = true)
    public void SetUp() {

        when(mockBoy.isRich()).thenReturn(true);
        when(mockBoy.getWealth()).thenReturn(10000000.0);
        when(mockBoy.isSummerMonth()).thenReturn(true);
        when(mockBoy.isPrettyGirlFriend()).thenReturn(true);
        when(mockBoy.getMood()).thenReturn(Mood.EXCELLENT);

        boys = new Boy[]{
            new Boy(Month.SEPTEMBER, 2000000.0),
            new Boy(Month.JUNE, 50000.0),
            new Boy(Month.MAY, 1500000.0),
            new Boy(Month.MAY, 2500000.0)
        };
    }
    @DataProvider(name = "Data for moodcheck create")
    public Object[][] ParamsMood(){
        return new Object[][]{
                {new Girl(true, false, boys[2]), Mood.EXCELLENT},
                {new Girl(true), Mood.GOOD},
                {new Girl(false, false, boys[2]), Mood.GOOD},
                {new Girl(false, true), Mood.NEUTRAL},
                {new Girl(false,true, boys[1]), Mood.NEUTRAL},
                {new Girl(false, false), Mood.I_HATE_THEM_ALL},
                {new Girl(true, false, mockBoy), Mood.EXCELLENT}
        };
    }

    @Override
    @Test(description = "check up Mood", dataProvider = "Data for moodcheck create", groups = "mood", priority = 0)
    public void MoodCheck(Human person, Mood expectedResult) {
        Mood actual = person.getMood();
        Assert.assertEquals(actual, expectedResult, "Get the mood of girl");
    }
    @DataProvider(name="Data for spend boyfriend money create")
    public Object[][] ParamsMoney(){
        return new Object[][]{
                {new Girl(true, false, boys[0]),0.0, 2000000.0},
                {new Girl(false, true, boys[0]), 100000.0, 1900000.0},
                {new Girl(false, true, boys[0]), 1900000.0, 0.0},
                {new Girl(true, false, boys[1]), 154000.0, 50000},
                {new Girl(true, false,boys[0]), 3000000.0, 0.0 },
                {new Girl(true,false, mockBoy),9000000.0, 10000000.0 },
        };
    }
    @Test(description = "check welth", dataProvider = "Data for spend boyfriend money create", groups = "spend", priority = 4)
    public void spendBoyFriendMoneyCheck(Girl girl, double spend, double expectedResult){
        girl.spendBoyFriendMoney(spend);
        double actual = girl.getBoyFriend().getWealth();
        Assert.assertEquals(actual, expectedResult,"check welth");
    }

    @DataProvider(name="Data for Rich Boyfriend check")
    public Object[][] ParamsRich(){
        return new Object[][]{
            {new Girl(true,false, boys[0]), true},
            {new Girl(true), false},
            {new Girl(false, true, boys[1]), false},
            {new Girl(false, false),false},
            {new Girl(false,false, mockBoy), true }
        };
    }
    @Override
    @Test(description = "check rich", dataProvider = "Data for Rich Boyfriend check", groups="rich", priority = 2)
    public void isRichCheck(Human person, boolean expectedResult){
        Girl g = (Girl)person;
        boolean actual = g.isBoyfriendRich();
        Assert.assertEquals(actual, expectedResult,"hi is the rich boy (may be)");
    }
    @DataProvider(name="Data for shoes create")
    public Object[][] ParamsShoes(){
        return new Object[][]{
                {new Girl(true,false, boys[0]), true},
                {new Girl(true,true, boys[0]), true},
                {new Girl(true), false},
                {new Girl(false), false},
                {new Girl(false, true, boys[0]), false},
                {new Girl(false, false),false},
                {new Girl(true, false, boys[1]),false},
                {new Girl(true,false, mockBoy),true},
        };
    }
    @Test(description = "check shoes",dataProvider = "Data for shoes create", groups = "rich", priority=3)
    public void isBoyFriendWillBuyNewShoesCheck(Girl girl, boolean expectedResult){
        boolean actual = girl.isBoyFriendWillBuyNewShoes();
        Assert.assertEquals(actual, expectedResult,"boyfriend will buy shoes");
    }

    @DataProvider(name="Data for fat girl create")
    public Object[][] ParamsFat(){
        return new Object[][]{
                {new Girl(true,true, boys[0]), false},
                {new Girl(true,false, boys[0]), false},
                {new Girl(true), false},
                {new Girl(), true},
                {new Girl(false, true, boys[0]), true},
                {new Girl(false, false),false},
                {new Girl(false, true),true},
        };
    }
    @Test(description = "check fat", dataProvider = "Data for fat girl create", groups = "pretty")
    public void isSlimFriendBecameFatCheck(Girl girl, boolean expectedResult){
        boolean actual = girl.isSlimFriendBecameFat();
        Assert.assertEquals(actual, expectedResult,"she is fat girl (may be)");
    }
}
